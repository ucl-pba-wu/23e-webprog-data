---
Week: xx
Content: xx, xx, xx, xx
Material: See links in weekly plan
Initials: NISI
---

# Uge xx - _dagens emne her_

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- ..
- ..

### Læringsmål

- ..
- ..

## Afleveringer

- ..
- ..

## Skema

Herunder er det planlagte program for ugen.

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 9:00  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer

- ..
