---
title: '23E PBa Web Udvikling'
subtitle: 'Fagplan for web programmering data'
filename: '23e_webprog_Data_lektionsplan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
email: 'nisi@ucl.dk'
left-header: \today
right-header: Fagplan for web programmering data
skip-toc: false
semester: 23E
hide:
  - footer
---

# Introduktion

Fagområdet indeholder datalagring, -modellering, -udveksling af datakilder ud fra anerkendte standarder samt datasikkerhed.

Faget er på 10 ECTS point.

# Læringsmål

**Viden**

Den studerende har udviklingsbaseret om praksis, anvendt teori og udviklingsmetoder indenfor:

- Relevante internet og webprotokoller
- Datalagring, -modellering, -udveksling og -sikkerhed
- Kvalitetssikring

Den studerende kan forstå og reflektere over:

- Udviklingsmetoder inden for webudvikling
- Webarkitektur og designmønstre

**Færdigheder**

Den studerende kan:

- Mestre alle udviklingens faser herunder planlægge, udvikle og idriftsætte webapplikationer baseret på konkrete udviklingsønsker, samt vurdere praksisnære og teoretiske problemstillinger og vælge og begrunde relevante løsningsmodeller i relation til udviklingen af webapplikationer.
- Vurdere og begrunde valget af et egnet programmeringssprog og relevante metoder til implementering af webapplikationer
- Mestre et egnet programmeringssprog til udvikling af webapplikationer
- Anvende og modellere datakilder samt begrunde og formidle løsningsforslag
- Implementere og vurdere webbrugergrænseflader samt begrunde og formidle løsningsforslag til samarbejdspartnere og brugere
- Anvende relevante teorier og metoder til kvalitetssikring af alle udviklingens faser

**Kompetencer**

Den studerende kan:

- Håndtere kompleks webudvikling og skal kunne håndtere komplekse og udviklingsorienterede situationer i webudvikling
- Selvstændigt indgå i fagligt og tværfagligt samarbejde med en professionel tilgang og påtage sig ansvar inden for rammerne af en professionel etik i relation til webprogrammering
- Identificere og strukturere egne læringsbehov og udvikle egne færdigheder og kompetencer i relation til webprogrammering

Se [Studieordningen sektion 2.1](https://esdhweb.ucl.dk/D21-1632339.pdf)

# Lektionsplan

Se ITSLearning

| Uge | Dato | Indhold |
| --- | --- | ---|
| 36| 20230904 8-15:13:45 | Introduktion til faget og data modellering |
| 37| 20230911 8-15:13:45 | SQL introduktion, datatyper og simple forespørgsler |
| 38| 20230918 8-15:13:45 | SQL forespørgsler og oprettelse |
| 39| 20230925 8-15:13:45 | Opdatering af database og forespørgsel til flere tabeller |
| 40| 20231002 8-15:13:45 | Funktioner, gruppering og komplekse forespørgsler |
| 43| 20231023 8-15:13:45 | Views, stored procedures, indexes og transaktioner | 
| 44| 20231030 8-15:11:30 | Projekt uge |
| 46| 20231113 8-15:15:30 | Database sikkerhed | * send rettelse til studieadm, 2 lekt. fra 3. november hertil
| 47| 20231120 8-15:13:45 | NoSQL og MongoDB |
| 48| 20231127 8-15:13:45 | MongoDB |
| 49| 20231204 8-15:13:45 | Database i webapplikationer del 1 |
| 50| 20231211 8-15:13:45 | Database i webapplikationer del 2 og Eksamens repetition |

# Studieaktivitets modellen

![study activity model](../images/wep_prog_data_studieaktivitetsmodel.png)

# Eksamen

Se [studieordningen afsnit 4.2 - 1. semesterprøve](https://esdhweb.ucl.dk/D22-2048535.pdf) 

## Eksamens- og afleveringsdatoer

- 12+15+16 Januar 2024 - 1. forsøg (aflevering af rapport 20. December 2024)
- 29 Januar - 2. forøsg (aflevering af rapport 22. Januar 2024) 
- 8 Februar - 3. Forsøg (aflevering af rapport 8. Februar 2024)

## Andet

Intet på nuværende tidspunkt
