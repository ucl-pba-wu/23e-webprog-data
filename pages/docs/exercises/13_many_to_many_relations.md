 

# Opgave 13 - many to many relationer (M:N)

## Information

Når 2 tabeller har en many to many relation kan relationene kun beskrives i det logiske ER diagram.  
DBMS kan ikke lave en sådan relation og derfor vil det være nødvendigt at bruge en junction/binding tabel i mellem de 2 tabeller.

Lad os tage et eksempel:

- En bog kan have mange forfattere
- En forfatter kan have skrevet mange bøger

I det logiske ER diagram er det ikke noget problem at lave det som 2 tabeller, faktisk er det fint i forhold til at holde overblik i diagrammet

![images/many_to_many.png](images/many_to_many.png)

Men fordi det ikke kan lade sig gøre at implementere fysisk i databasen er vi nød til at nebryde relationerne til one to many relationer (1:N)
Til det bruger vi en tabel til at binde de 2 tabeller sammen. En sådan tabel hedder en junction, eller binding tabel og indeholder udelukkende keys fra de 2 tabeller.

Et eksempel kan ses her:

![images/many_to_many_junction_table.png](images/many_to_many_junction_table.png)

Når databasen skal oprettes med SQL vil det se sådan her ud:

```SQL
CREATE DATABASE Many_test;
GO

USE Many_test;

CREATE TABLE Book (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    Title VARCHAR(255) NOT NULL,
    PublishDate DATE
)

CREATE TABLE Author (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    Firstname VARCHAR(255),
    Lastname VARCHAR(255)
)

CREATE TABLE BookHasAuthor (
    BookID INT,
    AuthorID INT,
    PRIMARY KEY (BookID, AuthorID),
    FOREIGN KEY (BookID) REFERENCES Book(ID),
    FOREIGN KEY (AuthorID) REFERENCES Author(ID)
);
```

Og data indsættes sådan:

```SQL
INSERT INTO Book(Title, PublishDate)
VALUES 
        ('Det forsømte forår', '1940-07-15'),
        ('Den forsvundne fuldmægtig', '1938-02-10'),
        ('Den døde mand', '1937-03-04'),
        ('Idealister', '1945-04-01'),
        ('Gertrud', '1910-01-01'),
        ('Der Steppenwolf', '1927-02-27'),
        ('Siddharta', '1922-03-18')

INSERT INTO Author (FirstName, Lastname)
VALUES
        ('Hans', 'Scherfig'),
        ('Hermann', 'Hesse')

INSERT INTO BookHasAuthor(BookID, AuthorID)
VALUES
        (1,1), (2,1), (3,1), (4,1), (5,2), (6,2), (7,2)
```

Læg mærke til at `BookHasAuthor` udelukkende indeholder keys og definerer derfor relationen mellem `Book` og `Author` tabellernes rækker.  

## Instruktioner

1. Skriv en `.sql` fil der opretter en database ud fra følgende fysiske ER diagram  
![images/data_modelling_many_to_many.png](images/data_modelling_many_to_many.png)
2. Lav en `.sql` fil der indsætter `dummy data` i databasen

## Links
