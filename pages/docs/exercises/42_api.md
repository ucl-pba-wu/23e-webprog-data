 

# Opgave 42 - API

## Information

Denne opgave giver dig en masse brugbar information som omhandler API arkitektur, best practice, udvikling, test og dokumentation.

Der er en del læsestof og du bør mens du læser besøge de links der ar angivet for at få mest mulig viden ud af opgaven.

Opgaven afsluttes med installation og test af en API ved hjælp af Postman

## Arkitektur

![images/api_architecture.png](images/api_architecture.png){ width="300", align=right }
**A**pplication **P**rogramming **I**nterface (**API**) er et interface med et sæt af funktioner der giver programmer og programmører adgang til specifikke features eller data fra applikationer, operativ systemer eller andre services.

En web API er en API der er web speficik og kan tilgås via HTTP(S) protokollen.  
Web API er et koncept og ikke en specifik teknologi.

Restful web api (REST API) er en web API baseret på REST principperne

Arkitekturen i en REST web api er i sin mest simple form illustreret her.

Som eksempel på hvordan dokumentationen for en API ser ud, hvilket inkluderer beskrivelse af endpoint URI'er og anvendte HTTP metoder kan du kigge på Spotifys API dokumentation  
[https://developer.spotify.com/documentation/web-api/](https://developer.spotify.com/documentation/web-api/)

## REST

**RE**presentational **S**tate **T**ransfer (**REST**) er en arkitektonisk stil og en samling af konventioner som tilsammen er defineret som en hjælp til at udvikle og organisere distribuerede systemer.

![images/api_rest.png](images/api_rest.png){ align=right }

- I REST baseret arkitektur bliver alt opfattet som en ressource
- Ressourcer identificeres ved hjælp af Uniform Ressource Identfiers (URI) også kaldet ressourcens endpoint
- En ressource tilgås via et fælles interface baseret på HTTP protokollens standarder og metoder

REST skal være platform uafhængig og 'stateless'.  
Platform uafhængig betyder at det skal være muligt at kommunikere med en REST API uafhængigt af klientens platform (se illustration).  
Stateless betyder at alle HTTP forespørgsler skal indeholde alle nødvendige informationer som API'en skal bruge for at forstå forespørgslen, sagt på en anden måde så skal API'en ikke gemme nogen informationer om den klient der anvender API'en.

**Hvad menes der med ressourcer ??**

I en REST kontekst er ressourcer alt hvad der kan navngives, en HTML side, et billede, en Person, en vejrudsigt etc.  
Ressourcerne definerer hvad en given service tilbyder og hvilken information der bliver overført.

Som eksempel er her dokumentationen for hvordan **Albums** ressourcen er defineret hos Spotify.

![images/api_album_spotify.png](images/api_album_spotify.png)

## HTTP CRUD og HTTP metoder

I **Opgave 36 - MongoDB CRUD** lærte du om CRUD i MongoDB kontekst.  
**CRUD** er en konvention som bruges til at beskrive basis operationerne **C**reate, **R**ead, **U**pdate og **D**elete

![images/api_crud.png](images/api_crud.png){ align=right}

**H**yper **T**ext **T**ransfer **P**rotocol (**HTTP**) er en protocol der opererer på lag 7 i [OSI modellen](https://en.wikipedia.org/wiki/OSI_model) (Applikations laget)
HTTP er sproget der tales mellem en web server og en klient f.eks en web browser.

HTTP definerer CRUD ved hjælp af verberne (metoderne) GET, POST, PUT, DELETE

I tabellen herunder er en oversigt over HTTP CRUD verberne og en beskrivelse af hvordan det foreslås at anvende dem i en API service

| HTTP Verb | Foreslået handling                                                                                                                |
| --------- | --------------------------------------------------------------------------------------------------------------------------------- |
| GET       | Access a resource in a read-only mode                                                                                             |
| POST      | Normally used to send a new resource into the server (create action)                                                              |
| PUT       | Normally used to update a given resource (update action)                                                                          |
| DELETE    | Used to delete a resource                                                                                                         |
| HEAD      | Not part of the CRUD actions, but the verb is used to ask if a given resource exists without returning any of its representations |

HTTP protokollen angiver en række af standard svar som kaldes status koder. Du er nok allerede bekendt med stuskoden `404` som betyder _Page not found_ eller `200` som betyder _OK_

Når du designer en API angiver du også hvilken status kode der skal returneres til klienter, her er det vigtigt at du vælger den korrekte statuskode i de enkelte endpoints.

HTTP statuskode kategorierne er angivet herunder

| HTTP status kode kategorier |
| --------------------------- |
| 1xx: Informational codes    |
| 2xx: Successful codes       |
| 3xx: Redirectional codes    |
| 4xx: Client Error Codes     |
| 5xx: Server Error Codes     |

En komplet oversigt over alle HTTP status koder kan findes her  
[https://developer.mozilla.org/en-US/docs/Web/HTTP/Status](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)

Svar fra et API endpoint bør altid indeholde information der kan hjælpe klienten med at løse eventuelle problemer, det kan for eksempel være en 404 med et link til din API dokumentation.

## API endpoint design guidelines

Når du designer endpoint's i din API er det vigtigt at navngive på en ensartet måde.

### Brug udsagn i stedet for verber

| Formål                          | HTTP metode | Forkert         | Korrekt           |
| ------------------------------- | ----------- | --------------- | ----------------- |
| Hent en liste over alle brugere | GET         | /getAllUsers     | /users            |
| Opret en ny bruger              | POST        | /createUser     | /users            |
| Slet en bruger                  | DELETE      | /deleteUser     | /users/10         |
| Hent balance data for en bruger | GET         | /getUserBalance | /users/11/balance |

### Relaterede ressourcer

Brug delressourcer, hvis en ressource er relateret til en anden ressource

- Returner en liste af chauffører for bil nummer 711
  `GET /cars/711/drivers/`
- Returner en specifik chauffør fra bil nummer 711
  `GET /cars/711/drivers/4`

### Brug flertals udsagn

Brug flertals udsagn i stedet for ental for at holde det simpelt.

For eksempel `/cars` istedet for `/car`, `/users` istedet for `/user` og `/settings` og ikke `/setting`

## GET må ikke ændre 'State'

Hvis du skal ændre 'state' altså data i en API skal du anvende `PUT`, `POST` og `DELETE` og IKKE `GET` selvom det er muligt

`GET` er kun til **READ**

### Brug af parametre til filtrering

Parametre i en query string er det der angives efter et `?` tegn.  
For eksempel `example.com/cars?color=red` til at få en liste af biler der er røde.

Parametre skal anvendes til at angive filtre i forespørgsler.

`GET /cars?color=red` eller `GET /users?name=tom`

Parametret håndteres i API'ets funtion, for eksempel sammen med en full-text search i MongoDB

### Pagination

Det er normalt at bruge pagination til at returnere resultater som "sider" af resultater. Det er brugbart fordi klienten ikke skal hente alle resultater fra en, potentielt stor, database og gemme dem i computerens hukommelse.  
En side i denne kontekst betyder bare x antal resultater, ad gangen for eks. 20 resultater for `GET /cars`

Du kan tilføje funktionalitet til din AP så det er klienten der angiver hvor mange resultater der returneres med `limit` og hvor i reultaterne der startes med ´offset´

Eksempel: `GET /cars?offset=10&limit=5` (starter ved reultat nummer 10 og returnerer 5 reultater pr. side)

Hvis du tilføjer den funktionalitet bør du sætte default offset til 0 og default limit til 20

## API Dokumentation

Da API kommunikation er programmatisk er god dokumentation essentiel. Man kan sige at god dokumentation = god DX (Developer Experience)

Dokumentation bør være tilgængeligt for alle brugere og er som regel udformet som en web side.  
Der findes værktøjer der generer API dokumentation og samtidig inkluderer mulighed for at afprøve en API.

Her er et par stykker:

- Postman API documentation tool [https://www.postman.com/api-documentation-tool/](https://www.postman.com/api-documentation-tool/)
- Swagger [https://swagger.io/solutions/api-documentation/](https://swagger.io/solutions/api-documentation/)

Et eksempel på hvordan swagger API dokumentation ser ud for en bruger kan ses og afprøves her:  
[https://petstore.swagger.io/](https://petstore.swagger.io/)

## API test (Postman)

Er API returnerer udelukkende tekst, den API du skal lære at bygge returnerer tekst i JSON format, hvilket er det mest anvendte.  
Et andet anvendt format er XML og der findes andre tekst formater til mere specialiserede formål.  

Det er muligt at teste en API i browseren men det er ikke optimalt npr der for eksempel skal sendes data i HTTP forespørgslens `Body`.  

Postman er et værktøj udviklet til at teste API og er meget velegnet at anvende under udvikling af en API eller hvis du skal anvende en API i en applikation du er ved at udvikle.

Postman kan hentes her [https://www.postman.com/downloads/](https://www.postman.com/downloads/)

The net ninja har en god video om at installere og bruge Postman [https://youtu.be/32y_UY1omwM](https://youtu.be/32y_UY1omwM)

## Instruktioner

1. Installer Postman (du behøver ikke at registrere dig for at bruge programmet)
2. Brug [https://petstore.swagger.io/](https://petstore.swagger.io/) dokumentation til at lave følgende API kald ved hjælp af Postman
    1. Brug en GET request til at finde alle pets der har status "pending"
    2. Brug POST til at tilføje et nyt pet til databasen
    3. Brug GET til at finde dit nye pet ved brug af dets ID
    4. Brug PUT til at opdatere dit pet med et nyt navn
    5. Brug GET til at bekræfte at dit pet er opdateret 
    6. Brug DELETE til at slette dit pet
3. Der findes mange gratis API'er på nettet, her er en liste: [https://apipheny.io/free-api/](https://apipheny.io/free-api/)
    1. Brug Postman til at hente et random catfact fra [https://catfact.ninja](https://catfact.ninja)
    2. Find en aktivitet for 4 personer fra [https://www.boredapi.com/documentation](https://www.boredapi.com/documentation) 
    3. Se om [https://nationalize.io/](https://nationalize.io/) kan finde ud af hvor du kommer fra, baseret på dit navn - du skal selvfølgeig bruge postman
4. Find et API på [https://apipheny.io/free-api/](https://apipheny.io/free-api/) listen, afprøv det og instruer personen der sidder ved siden dig i at bruge det med Postman.

## Links
