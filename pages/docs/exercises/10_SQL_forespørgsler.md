 

# Opgave 10 - SQL forespørgsler

## Information

SQL gør det muligt at fremsøge data i en relationel database.
For at kunne gøre det er det nødvendigt at kende til:

- SELECT
- DISTINCT
- WHERE
- AND, OR og NOT
- ORDER BY
- BETWEEN
- LIKE
- TOP 

i SQL.

Du får i denne opgave mulighed for at læse, forstå og afprøve ovenstående SQL termer.  
I opgave 8 kan du træne yderligere ved at lave forespørgsler i WorldDB databasen.  

## Instruktioner

1. Læs om og prøv SELECT [https://www.w3schools.com/sql/sql_select.asp](https://www.w3schools.com/sql/sql_select.asp)
2. Læs om og prøv DISTINCT [https://www.w3schools.com/sql/sql_distinct.asp](https://www.w3schools.com/sql/sql_distinct.asp)
3. Læs om og prøv WHERE [https://www.w3schools.com/sql/sql_where.asp](https://www.w3schools.com/sql/sql_where.asp)
4. Læs om og prøv AND, OR og NOT [https://www.w3schools.com/sql/sql_and_or.asp](https://www.w3schools.com/sql/sql_and_or.asp)
5. Læs om og prøv ORDER BY [https://www.w3schools.com/sql/sql_orderby.asp](https://www.w3schools.com/sql/sql_orderby.asp)
6. Læs om og prøv BETWEEN [https://www.w3schools.com/sql/sql_between.asp](https://www.w3schools.com/sql/sql_between.asp)
7. Læs om og prøv LIKE [https://www.w3schools.com/sql/sql_like.asp](https://www.w3schools.com/sql/sql_like.asp)
8. Læs om og prøv TOP [https://www.w3schools.com/SQL/sql_top.asp](https://www.w3schools.com/SQL/sql_top.asp)

## Links

Den officielle Microsoft dokumentation for ovenstående kan findes i Select sektionen [https://docs.microsoft.com/en-us/sql/t-sql/queries/select-transact-sql?view=sql-server-ver16](https://docs.microsoft.com/en-us/sql/t-sql/queries/select-transact-sql?view=sql-server-ver16)
