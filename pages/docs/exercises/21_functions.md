 

# Opgave 21 - Funktioner i T-SQL

## Information

I T-SQL kan du bruge funktioner til at manipulere data inden det indsættes til eller forespørges fra databasen.
Det er muligt at skrive sine egne funktioner, men typisk vil det være de indbyggede funktioner i T-SQL man anvender.  

Overordnet findes der 4 kategorier af funktioner:

- String funktioner, som bruges til at manipulere string data typer
- Numeriske funktioner, bruges til at manipulere tal
- Dato funktioner, bruges til dato data typer
- samle funktioner (aggregate), bruges til at summere resultater

Dokumentationen for T-SQL funktioner kan findes her:  
[https://learn.microsoft.com/en-us/sql/t-sql/functions/functions](https://learn.microsoft.com/en-us/sql/t-sql/functions/functions)

Funktioner i T-SQL har den samme opbygning som funktioner du f.eks kender fra matematik eller et andet programmeringssprog.  

I matematik kan en funktion f.eks være:

$f(x)=x+2$

Hvor $x$ er input til funktionen og det funktionen udfører er at lægge $2$ til $x$.  
Hvis $x$ f.eks er $10$ vil det funktionen returnerer være $12$ som er resultatet af $x+10$

$12=10+2$

Hvor matematik kun giver mulighed for at lave numeriske funktioner kan vi i programmering lave funktioner der opererer på de datatyper der er implementeret i det pågældende programmeringssprog.

En funktion der sammensætter to strings i *javascript* kan f.eks være:

```js
function concatenateTwo(word1, word2){
    return word1+word2
}
```

I programmering kalder vi definitionerne for input til funktioner for parametre.  
Når en funktion bruges kaldes input for argumenter.  
`concatenateTwo` funktionen har defineret 2 parametre, *word1*, og *word2* som er de argumenter der skal angives når funktionen kaldes:  

```js
result = concatenateTwo('I am ', 'a function result')
console.log(result)

'I am a function result'
```

### String Funktioner
En oversigt over alle T-SQL string funktioner kan findes her:  
[https://learn.microsoft.com/en-us/sql/t-sql/functions/string-functions-transact-sql](https://learn.microsoft.com/en-us/sql/t-sql/functions/string-functions-transact-sql)

T-SQL String funktioner opererer som nævnt på string datatyper.  

Et eksempel på en T-SQL string funktion er `CONCAT` der sætter to ord sammen i en kollonne når resultatet af forespørgslen vises.

```SQL
SELECT CONCAT(firstname, ' ', lastname) AS FullName
FROM Employee
```

Til forveksling ligner `CONCAT` eksemplets syntaks, syntaksen fra javascript eksemplet tidligere hvis du sammenligner.  
`CONCAT` er navnet på funktionen og i parentesen, er der defineret 2 parametre til `CONCAT` funktionen. 
Det er vigtigt at nævne typerne der bruges i funktionen, argumenterne skal være string typer, det vil f.eks ikke virke med `INT`.  
Outputtet er string typen `NVARCHAR`

--------

Et andet eksempel på en T-SQL string funktion er `REPLACE`

Hvis `REPLACE` funktionen skrives i javascript kan den se sådan her ud:

```js
function stringReplacement(string_expression, string_pattern, string_replacement){
    string_pattern = new RegExp(string_pattern, 'g')
    return string_expression.replace(string_pattern, string_replacement)
}
```

Javascript funktionen kaldes (anvendes) ved at give de 3 parametre  
*string_expression, string_pattern, string_replacement*

```js
colors = 'green, yellow, blue, cyan, blue' 
str_exp = 'this text is abuot a woman and also abuot her cat' //tekststreng (string_expression)
result = stringReplacement(str_exp, 'abuot', 'about')
console.log(result)

'this text is about a woman and also about her cat'
```

I T-SQL er syntaksen for brug af `REPLACE`  

```SQL
REPLACE(string_expression, string_pattern, string_replacement)
```
Hvis du sammenligner med syntaksen fra javascript eksemplet kan du se en lighed, REPLACE er navnet på funktionen og i parantesen er der defineret 3 parametre til `REPLACE` funktionen.

Et eksempel på brug af T-SQL `REPLACE` i en tænkt database kan være følgende

```SQL
UPDATE products
SET description =
REPLACE(description,'abuot','about');
```

Eksemplet opdaterer description kolonnen og erstatter ved brug af `REPLACE` funktionen, ordet *abuot* med *about*  

Datatypen der returneres fra `REPLACE` er af datatypen NVARCHAR hvis input er af denne type, ellers returneres resultatet som VARCHAR.  
Argumenternes datatype kan du se i dokumentationen her:   
[https://learn.microsoft.com/en-us/sql/t-sql/functions/replace-transact-sql](https://learn.microsoft.com/en-us/sql/t-sql/functions/replace-transact-sql)

### Numeriske/matematiske funktioner
En oversigt over alle T-SQL matematiske funktioner kan findes her:  
[https://learn.microsoft.com/en-us/sql/t-sql/functions/mathematical-functions-transact-sql](https://learn.microsoft.com/en-us/sql/t-sql/functions/mathematical-functions-transact-sql)

Et eksempel på en matematisk funktion i T-SQL er `ROUND`:  

```SQL
SELECT ROUND(1.298, 1);
--Result: 1.3
```
som afrunder det første argument til det antal decimaler der er angivet i 2. argument.

```SQL
SELECT CEILING(41.16);
--Result: 42
```
`CEILING` returnerer mindste heltal der er større eller lig med argumentet der angives når funktionen kaldes.

### Dato og tid funktioner
En oversigt over alle dato/tid funktioner kan findes her:  
[https://learn.microsoft.com/en-us/sql/t-sql/functions/date-and-time-data-types-and-functions-transact-sql](https://learn.microsoft.com/en-us/sql/t-sql/functions/date-and-time-data-types-and-functions-transact-sql)


Et eksempel på brug af `DATEDIFF`:
```SQL
SELECT DATEDIFF(year, '2018-12-31', '2019-05-01') as DateDiff;
```
`DATEDIFF` returnerer forskellen på datoerne i 2. og 3. argument, som et heltal i det første arguments angivne interval, i dette tilfælde, år (*year*).  

|  | DateDiff |
| --- | --- |
|1 | 1 |

`GETDATE` er et andet eksempel på en dato/tid funktion som returnerer systemts dato og tid

```SQL
SELECT GETDATE();
```

|  | SystemDate |
| --- | --- |
|1 | 2022-09-22 10:13:00.573 |

### Aggregate funktioner

En oversigt over alle aggregate funktioner kan findes her:
[https://docs.microsoft.com/en-us/sql/t-sql/functions/aggregate-functions-transact-sql](https://docs.microsoft.com/en-us/sql/t-sql/functions/aggregate-functions-transact-sql)

Aggregate betyder direkte oversat til dansk *samlet*. Aggregate funktioner udfører beregninger på en gruppe af resultater og bruges derfor ofte sammen med `GROUP BY`.
Aggregate funktioner medtager ikke resultater i gruppen som har `NULL` værdien

`SUM` bruges til at returnere summen af en gruppe af værdier:
```SQL
SELECT SUM(column_name) FROM table_name;
```

`COUNT` tæller antallet af værdier i gruppen:

```SQL
SELECT COUNT(*) FROM employees;
```

Her er 3 aggregate eksempler på `COUNT` brugt på World databasen:

```SQL
SELECT count(*) FROM country;
SELECT count(indepyear) FROM country;
SELECT count(DISTINCT continent) FROM country;
```

Og her et eksempler på brug af `AVG` `MIN` og `MAX` aggregate funktionerne:

```SQL
USE World;

SELECT AVG(population)
FROM country
WHERE region = 'Western Europe';

SELECT MIN(population), MAX(population)
FROM country
WHERE region = 'Western Europe';
```

## Instruktioner

Nu er det jeres tur til at gå på opdagelse i T-SQL sprogets funktioner.  
I skal lave opgaven som gruppe og sammen beslutte samt udføre forespørgslerne i Northwind databasen

1. Vælg en funktion fra hver kategori (String, numerisk og date)
2. Lav 1 forespørgsel for hver katagori, brug Northwind databasen, i skal altså tilsammen lave 3 forespørgsler
3. Forbered en kort demo af jeres forespørgsler som i kan vise på klassen

## Links
