 

# Opgave 4 - Database design

## Information

Database design bør altid tage udgangspunkt i hvordan data skal bruges og repræsenteres.  
Når relationelle databaser skal designes er der et par grundregler der er gode at huske:

| Hvad                                             | Hvorfor?                                         |
| ------------------------------------------------ | ------------------------------------------------ |
| Data i tabeller skal være unikt, ingen redundans | For at forsimple forespørgsler                   |
| Data skal være konsistent                        | Sikrer hurtigere forespørgsler                   |
| Gem kun data som er nødvendigt                   | Reducerer ændringer senere i udviklingsprocessen |

I database design processen er det ofte godt at være flere til at analysere og diskutere behovet for data og hvordan data skal bruges i en given applikation.

Design processen kan nedbrydes til 3 faser.

![images/database_dev_lifecycle.png](images/database_dev_lifecycle.png)
[https://www.guru99.com/database-design.html](https://www.guru99.com/database-design.html)

Modellen kan anses som iterativ og gentages dermed hvis kravene til databasen ændres senere i udviklingsprocessen.

1. **Analyse af databehov**  
   Her analyseres hvilke behov for data en given applikation vil have. Det kan gøres på forskellige måder, men overordnet udføres følgende:
    - Analysér applikationen. Det kan gøres ud fra en design mockup hvis det er til en web løsning, hvis ikke så er det nødvendigt at have en dialog med kunden om hvad de har behov for.
    - Definér problemer, muligheder og begrænsninger. Tænk over hvordan data skal forespørges i applikationen, hvor meget data skal gemmes osv.
    - Definér formålene med databasen
    - Bliv enige om omfanget, altså hvad skal databasen indeholde og kunne ?
2. **Modellering af Logisk og fysisk model**  
    ![images/db_design_relationer.png](images/db_design_relationer.png)
    
    Her defineres den relationelle datamodel. Et godt værktøj (og det i skal bruge) er Entity Relation (ER) diagrammer. I dennes fase skal følgende defineres:
    
    - Enheder/tabeller (entities) - f.eks. personer, steder eller ting som du gerne vil gemme informationer om i databasen. Et biblioteks system vil sandsynligvis have brug for enhederne: Bog, bibliotek og låner (og sikkert mange flere)
    - Egenskaber (attributes) - beskriver detaljer omkring enheden. En bog har f.eks en titel, side antal og et isbn nummer.
    - Relationer (relationship) - beskriver hvordan enheder er relateret, en bog har f.eks et forlag.  
    

3. **Implementering** 
    Her oprettes databasen med tabeller, datatyper og relationer. Den fysiske model anvendes her for at sikre at designet overholdes.  
    Ofte skrives SQL kommandoerne i et script så databasen kan gendannes hvis det skulle blive nødvendigt.  
    Et script er også praktisk hvis der senere skal foretages ændringer i databasen.


## Instruktioner

1. Læs opgaven og den linkede artikel igennem.
2. Lav i gruppen en checkliste med udgangspunkt i jeres nye viden.  
Checklisten kan i bruge når i skal designe databaser i fremtiden.

## Links

...
