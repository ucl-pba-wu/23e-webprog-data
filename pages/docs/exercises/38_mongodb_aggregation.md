 

# Opgave 38 - MongoDB aggregering

## Information

**aggregere**  
*samle sammen, ofte i en ordnet mængde fx data, enheder eller informationer*  
*[Den dansk ordbog](https://ordnet.dk/ddo/ordbog?query=aggregere&entry_id=11000932)*

Aggregering er ikke kun en MongoBD term, ordets betydning beskriver ret godt hvad det betyder.  
I MongoDB kontekst bruger vi det der kaldes en *Aggregation pipeline* som kan forstås som en samling af MongoDB kommandoer ogsp kaldet stages.

Basis syntaks for aggregering er:

```js
db.collection.aggregate( [ { <stage> }, ... ] )
```

Basis syntaksen viser at hver stage er et objekt i et array

Når vi aggregerer data vil det ofte starte med matche (match stage) dokumenter i første stage og derefter gruppere (group stage) ønsket felt, i group stage vælges også hvad grupperingen skal kaldes (på samme måde som alias i SQL)

![images/mongo_aggregation.png](images/mongo_aggregation.png)

![images/mongo_aggregation_2.png](images/mongo_aggregation_2.png)

### Pipeline stage operatorer

Der findes forskellige operatorer som kan bruges i en pipelines stages, her er en oversigt og sammenligning med SQL termer

|     MongoDB Operators    |     SQL equivalent                    |
|--------------------------|---------------------------------------|
|     $project             |     SELECT                            |
|     $match               |     WHERE   or HAVING                 |
|     $group               |     GROUP   BY                        |
|     $sort                |     ORDER   BY                        |
|     $limit               |     LIMIT   / TOP                     |
|     $count               |     COUNT()                           |
|     $unwind              |     Deconstructs   an array field     |
|     $out                 |     Writes   results to collection    |
|     $lookup              |     JOIN                              |


## Instruktioner

Alle kommandoer skal gemmes i en fil der hedder `restaurants_aggregate_solution.js`

1. Hent filen: [https://ucl-pba-wu.gitlab.io/22e-wu-wep-data/files/mongodb/Datasets/restaurants.js](https://ucl-pba-wu.gitlab.io/22e-wu-wep-data/files/mongodb/Datasets/restaurants.js) du kan højreklikke på den i din browser og vælge *gem som*
2. Opret restaurants databasen i mongodb med mongosh
3. Løs følgende delopgaver ved brug af aggregation pipeline i mongosh, hver delopgave har angivet korrekt output, som du kan bruge til at kontrollere dit output  
    1. Vis antal restauranter pr. køkken (cousine)  
    *Korrekt output*
    ```js
    [
      { _id: 'American', total: 2 },
      { _id: 'Danish', total: 3 },
      { _id: 'Italian', total: 3 }
    ]
    ```
    2. Vis antallet af restauranter pr. by, højeste antal skal være først og kun de 3 første resultater skal vises  
    *Korrekt output*  
    ```js
    [
      { _id: 'Nyborg', restprcity: 2 },
      { _id: 'Odense', restprcity: 2 },
      { _id: 'Svendborg', restprcity: 2 }
    ]
    ```
    3. Vis hvor mange gæster det samlede antal restauranter i Odense har plads til  
    *Korrekt output*  
    ```js
    [ { capforOdense: 160 } ]
    ```
    4. Vis den højeste max pris blandt alle restauranterne   
    *Korrekt output*  
    ```js
    [ { _id: null, max_price_all: 1000 } ]
    ```
    5. Vis antal inspektioner pr. inspetør   
    *Korrekt output*  
    ```js
    [
      { _id: 'Lars Larsen', count: 4 },
      { _id: 'John Smith', count: 3 },
      { _id: 'Morten Gaard', count: 2 },
      { _id: 'Adam Ansger', count: 2 },
      { _id: 'Morten Olsen', count: 1 },
      { _id: 'Morten Larsen', count: 1 }
    ]
    ```
    6. Vis gennemsnits score pr. restaurant  
    *Korrekt output*  
    ```js
    [
      { avgscore: 4.5, name: 'Spis' },
      { avgscore: 3, name: 'Nybo Kro' },
      { avgscore: 3, name: 'Bar Fredo' },
      { avgscore: 4, name: 'Italia' },
      { avgscore: 3, name: 'Munkebo Kro' },
      { avgscore: 4, name: 'Grill & Flames' },
      { avgscore: 3, name: 'Bar 99' },
      { avgscore: 3, name: 'Pizzeria Paula' }
    ]
    ```

## Links

- MongoDB aggregation pipeline [https://www.mongodb.com/docs/manual/core/aggregation-pipeline/](https://www.mongodb.com/docs/manual/core/aggregation-pipeline/)
