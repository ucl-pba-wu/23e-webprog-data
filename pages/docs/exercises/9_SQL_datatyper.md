 

# Opgave 9 - SQL data typer

## Information

Overblik over SQL datatyper og hvorfor det er vigtigt at vælge den rigtige.
Brug T-SQL reference til at finde info om typerne.

SQL anvender datatyper til at gemme data.  
Det er vigtigt at vælge den rigtige datatype i designfasen af følgende årsager:

- Datatypen fortæller hvordan SQL interagerer med den gemte data
- Den rigtige datatype understøtter en effektiv database samt optimisering af blandt andet databasens størrelse
- Valg af korrekt datatype sikrer konsistens og gør validering af data mulig
- Datatyper kan være vanskelige at ændre når databasen er oprettet uden tab af data

Når du skal vælge datatyper skal du overveje:

- **Data størrelse** Hvad er de største og mindste værdier du ønsker at gemme i datatypen. F.eks vil alder ofte være `int` men måske er det bedre at bruge `tinyint` som går fra 0 - 255 ?
- **Anvendelse** Hvordan skal værdierne bruges i databasens applikationer ? Alder er måske en dårlig ide ifht. hvordan data skal bruges, det er nok bedre at bruge fødselsdato men hvilket datoformat er så det rigtige i forhold til hvrdan data skal bruges ?
- **Korrekt data opbevaring** Som nævnt kan det være svært at ændre datatypen når databasen er i drift uden at tabe data, derfor er designet meget vigtigt.

## Instruktioner

1. Brug dette link [https://docs.microsoft.com/en-us/sql/t-sql/data-types/data-types-transact-sql](https://docs.microsoft.com/en-us/sql/t-sql/data-types/data-types-transact-sql) og besvar følgende:
    - Hvordan er datatyperne kategoriseret (noter kategorierne)
    - Giv eksempler på anvendelse af 5 forskellige datatyper.
    - Hvor meget fylder *varchar* i forhold til *char* ?
    - Hvad er min og maks værdierne i *int*
    - Hvad er min og maks værdierne i *money*
    - Hvor mange datoformater findes der ? og er de lige nøjagtige ?
    - Hvad er en grundlæggende ogvigtig forskel på *fixed* f.eks. *int* og *float* f.eks. *real* datatyperne ?
    - Hvilke værdier kan *bit* antage ?

## Links
