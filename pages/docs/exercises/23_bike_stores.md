 

# Opgave 23 - Bike stores database

## Information

I denne opgave skal du installere en database der hedder bike stores og lave forespørgsler for at træne det du har lært i opgave **Opgave 21 - Funktioner i SQL** og **Opgave 22 - Gruppering af data**

Databasens skema er angivet i nedenstående ER diagram

![images/bike_stores_schema.png](images/bike_stores_schema.png)

## Instruktioner

1. download `Sample DB Bikestores.zip` fra ITSLearning, pak filen ud
2. Installer databasen og tabeller med filen `1 - BikeStores Sample Database - create objects.sql`
3. Fyld data i dabasen med filen `2 - BikeStores Sample Database - load data.sql`

Lav følgende forespørgsler i `BikeStores` databasen:

1. Hvor mange kunder er der i alt i databasen ?

    _resultat:_

    |     | total_customers |
    | --- | -------------------- |
    | 1   | 1445                |

2. Vis alle ordrer hvor shipped date er 2 dage senere end required date

    _resultat (2 første + 2 sidste linjer vist):_

    | |  order_id   | required date | shipped_date |
    | | --- | -------------------- | --- |
    | 1| 15   | 2016-01-10               | 2016-01-12 |
    | 2|21| 2016-01-16 | 2016-01-18 |
    | ...|...|...|...|
    | 152| 1467 | 2018-03-27| 2018-03-29|
    | 153|1469| 2018-03-28| 2018-03-30|

3. Hvad er gennemsnits list price for butikkens produkter, vis resultatet som heltal

    _resultat:_

    |     | avg_list_prod_price |
    | --- | -------------------- |
    | 1   | 1521                |

4. Hvor mange kunder er der pr. state ?

    _resultat:_

    | | state	| customerPerState |
    | --- |---|---|
    | 1| TX	|  142 |
    | 2| NY	|  1019 |
    | 3| CA	|  284 |

5. Vis antal produkter grupperet efter model year

    _resultat:_

    | | model_year	| numberOfProducts |
    | --- |---|---|
    | 1| 2016	|  26 |
    | 2| 2017	|  85 |
    | 3| 2018	|  204 |
    | 4|2019| 6 |

**BONUS hvis du vil udfordres:**

1. Vis brand name antal produkter pr. brand. Vis brand med flest produkter først. (kræver `JOIN`)

    _resultat (første + sidste linje vist):_

    | | brand_name	| NumberOfProducts |
    | --- |---|---|
    | 1| Trek	|  135 |
    | ... | ...	| ... |
    | 9| Ritchey| 1 |

2. Vis ordre (order_id) i en kolonne og totalpris (totalprice) pr. ordre i en anden kolonne (kræver `JOIN`)

    _resultat (2 første + 2 sidste linjer vist):_

    | | order_id	| total_price |
    | --- |---|---|
    | 1| 1	|  10231.0464 |
    | 2| 2	|  1697.9717 |
    | ... | ...	| ... |
    | 1614 | 1614| 6104.0354 |
    | 1615| 1615| 6516.9667 |


## Links

- [https://www.sqlservertutorial.net/sql-server-sample-database/](https://www.sqlservertutorial.net/sql-server-sample-database/)
