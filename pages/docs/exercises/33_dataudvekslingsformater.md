 

# Opgave 33 - Dataudveksling mellem applikationer på nettet (JSON og XML)

## Information

Når data skal udveksles mellem applikationer på nettet er der behov for at strukturere data på en måde som både afsender og modtager forstår.  

Til dette bruges primært 2 formater, Extensible Markup Language (XML) [https://da.wikipedia.org/wiki/XML](https://da.wikipedia.org/wiki/XML) og JavaScript Object Notation (JSON) [https://en.wikipedia.org/wiki/JSON](https://en.wikipedia.org/wiki/JSON)

Både XML og JSON er konstrueret så det er læsbart for både mennesker og maskiner. Begge formater skrives i klartekst modsat f.eks binære formater som ikke er læsbare for mennekser, kun maskiner.  

XML er det "gamle" udvekslingsformat hvor JSON er det "nye". At XML er det "gamle" format, betyder ikke at det er udgået og ikke anvendes mere.  
XML er en standard der er afprøvet og kendes af mange + XML understøtter flere datattyper end JSON.  

Dog er XML mere omfangsrigt at skrive og det har et større "overhead", det vil sige data skrevet som XML fylder mere end JSON.  

### Schema

Et schema kender du allerede fra relationelle databaser, når du modellerer din database som et fysisk ER diagram konstruerer du databasens schema.
Schema er det der bestemmer hvad databasen må indeholde, både i form af tabellernes felter men også af de tilladte datatyper.

Forklaret på en populær måde er et schema som en mad opskrift, f.eks er her en opskrift på 10 stk. pandekager:

|Mængde|Ingrediens|
|---|---|
|150 g.|Hvedemel|
|4 dl|Mælk|
|50 g|Smør|
|3|Æg|
|1/2 tsk|Salt|

Pandekage skemaet tilader kun de ingredienser der er defineret i skemaet, du må f.eks ikke putte hakket oksekød eller levertran i pandekagerne. 

Skemaet kan bruges når data modtages til at validere om data lever op til reglerne i skemaet.  

### XML

XML er et markup sprog på samme måde som du kender fra HTML, det benytter start \<tags\> og slut \</tags\>.

Her er et eksempel på et xml dokument (læg mærke til linje 4)

```XML linenums="1" title="XML dokument"
<?xml version="1.1"?>
<note>
  <to>Tove</to>
  <number>hello</number>
  <from>Jani</from>
  <heading>Reminder</heading>
  <body>Don't forget me this weekend!</body>
</note> 
```

XML dokumentet kan valideres mod følgende XML schema

```xml linenums="1" title="XML schema"
<?xml version="1.1"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
  
<xs:element name="note">
  <xs:complexType>
    <xs:sequence>
      <xs:element name="to" type="xs:string"/>
      <xs:element name="number" type="xs:nonNegativeInteger"/>
      <xs:element name="from" type="xs:string"/>
      <xs:element name="heading" type="xs:string"/>
      <xs:element name="body" type="xs:string"/>
    </xs:sequence>
  </xs:complexType>
</xs:element>

</xs:schema> 
```

Hvis du prøver at validere XML dokumentet på [https://www.liquid-technologies.com/online-xsd-validator](https://www.liquid-technologies.com/online-xsd-validator) vil du få følgende fejl meddelelse:

`cvc-datatype-valid.1.2.1: 'hello' is not a valid value for 'integer'.`

Fejlen skyldes at `number` elementer forventer datatypen nonNegativeInteger.

Skemaet referer [http://www.w3.org/2001/XMLSchema](http://www.w3.org/2001/XMLSchema) som er W3 organisationens standard for XML schema.  
Det vil sige regler for hvordan et XML Schema skal opbugges og hvad det kan indeholde.  
Indholdet er defineret i hhv.  
[http://www.w3.org/2009/XMLSchema/XMLSchema.dtd](http://www.w3.org/2009/XMLSchema/XMLSchema.dtd)  
og  
[http://www.w3.org/2009/XMLSchema/datatypes.dtd](http://www.w3.org/2009/XMLSchema/datatypes.dtd)

Det er ikke påkrævet at anvende et skema sammen med XML men det er anbefalet.

#### XML kendetegn

- XML er et markupsprog
- XML bruges til at hierakisk data
- XML er ikke software eller hardware afhængigt
- Kommentarer er tilladt i XML dokumenter
- XML inkluderer mellemrum (whitespace)
- XML er "case sensitive" altså er der forskel på store og små bogstaver
- XML er "datatungt" og kræver et åbne og lukke \<tag\> pr. data enhed

### JSON

JSON er et data udveklslings format der har samme opbygning som javascript objekter, men også som dictonaries i Python.  

Her er et eksempel på JSON:

```json linenums="1" title="products.json"
{ "products": 
  [
    {
       "product": "Felix Yummy Cat Food",
       "price": 11.99,
       "weight": "500g"
    },
    {
       "product": "Max's Favorite Dog food",
       "price": 15.99
    }
  ]
}
```

JSON er som navnet antyder objekt notation, hvilket betyder et opbyningen er den samme som javascript objekter.  
Et JSON (og JS) objekt starter og slutter med tuborg klammer (curly braces) `{}` og er opbygget af `key:value` par, hvor `key` er navnet på data feltet og `value` er data feltets værdi.    

Objektet kan indeholde datatyperne: 

|Type|Beskrivelse|Eksempel|
|---|---|---|
|Number|hel eller decimal tal| 7913.14|
|String|skrives i "" og escapes hvis der skal bruges anførselstegn | "Hej webdev" eller "Hej \\"webdev\\""|
|Boolean|true eller false|true|
|Array|værdier i ordnet rækkefølge, kan være alle andre datatyper|[ "Blue", "Deeppink", 40, "Green"]
|
|Object|key:value par i ikke ordnet rækkefølge|{"color":"blue", "age": 40}|
|Null|Ikke udfyldt værdi|null|

Et eksempel på brug af alle datatyper er herunder:

```json linenums="1" title="person.json"
{
 "name": "Jane",
 "age": 34,
 "address": {
   "street": "Seebladsgade 1",
   "city": "Odense C"
 },
 "children": [
   "John",
   "Charles"
 ],
 "education": [
   {
     "name": "Web Dev",
     "startdate": "2020-08-01",
     "finishdate": null
   },
   {
     "name": "MMD",
     "startdate": "2018-08-01",
     "finishdate": "2020-06-30"
   }
 ]
}
```
Det er muligt at "neste" datatyper, det vil sige at et array kan indeholde alle de andre datatayper, et objekt kan indeholde andre objekter (og de andre datatyper) etc.

På den måde kan JSON objekter blive temmelig omfattende og "dybe" når de indeholder meget data.

JSON kan "parses" altså oversættes, til JavaScript okjekter med den indbyggede `JSON.parse()` funktion.  
JavaScript objekter kan parses til JSON med `JSON.stringify()` JavaScript funktionen. 

```js linenums="1" title="student_parse.js"
let json_string = '{"name":"Jane","age":34,"address":{"street":"Seebladsgade 1","city":"Odense C"}}'
const student = JSON.parse(json_string)
console.log(student.name)
console.log(student.address.street)

JSON.stringify(student)
console.log(student)

/*
$ node example.js
Jane
Seebladsgade 1
{
  name: 'Jane',
  age: 34,
  address: { street: 'Seebladsgade 1', city: 'Odense C' }
}
*/
```

JSON kan godt implemetere skemaer som eksemplet med XML, det giver nogle af de samme fordele og gør det muligt at validere JSON data i forhold til et defineret skema.  
Læs mere om JSON schema standarden her  
[http://json-schema.org/](http://json-schema.org/)  
Et brugbart eksempel på brug af JSON Schema kan ses her  
[https://json-schema.org/understanding-json-schema/about.html](https://json-schema.org/understanding-json-schema/about.html)  

En online JSON schema validator er her [https://www.liquid-technologies.com/online-json-schema-validator](https://www.liquid-technologies.com/online-json-schema-validator)

Det er også muligt at validere om JSON syntaksen overholdes, det er ofte brugbart ;-)

JSON Beautify & validate [https://jsonbeautify.com/](https://jsonbeautify.com/)


#### JSON kendetegn

- JSON er et letvægts data udvekslings format
- JSON syntaks har en simpel struktur og få datatyper
- JSON er ikke software eller hardware afhængigt
- JSON starter og slutter altid med `{}`, medmindre der er flere objekter, så indkapsles de i `[]`
- filtypen er `.json`
- JSON består af `key:value` par
- Flere `key:value` par adskilles med komma `,`
- `keys` skal omsluttes af anførseletegn `""`
- JSON kan ikke indeholde kommentarer

## Instruktioner

1. Prøv værktøjet [https://www.liquid-technologies.com/online-xsd-validator](https://www.liquid-technologies.com/online-xsd-validator)  
og se om du kan rette fejlen ved at ændre enten 
`XML schema` eller `XML dokument`
2. Lav det viste mockup om til en valid JSON fil. Brug VS Code til at skrive JSON objektet og valider det med [https://jsonbeautify.com/](https://jsonbeautify.com/) eller anden validator 
![images/json_mockup.png](images/json_mockup.png)
3. Lav en `.js` fil og prøv kodeeksemplet `student_parse.js` med dit eget JSON objekt

## Links

- W3schools XML [https://www.w3schools.com/xml/default.asp](https://www.w3schools.com/xml/default.asp)
- W3 XML standard [https://www.w3.org/standards/xml/](https://www.w3.org/standards/xml/)
- W3 Schools JSON [https://www.w3schools.com/js/js_json_intro.asp](https://www.w3schools.com/js/js_json_intro.asp)
- ECMA JSON Data interchange standard [http://www.json.org/json-en.html](http://www.json.org/json-en.html)
