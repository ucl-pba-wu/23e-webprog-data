 

# Opgave 2 - Databasens historie

## Information

I denne opgave skal i undersøge databasens historie.

Opgaven er en gruppe opgave og i skal huske at notere jeres svar mens i arbejder. 

## Instruktioner

1. Brug "tænk, par, del" til at besvare følgende - 30 minutter til at tænke, 10 minutter til par, 5 minutter til del  
    1. I hvilket årti kom de første Database Management Systemer (DBMS) ?
    2. Hvad hed forfatteren til den første artikel om "den relationelle datamodel" og i hvilket årti udkom artiklen ?
    3. I hvilket årti blev Structured Query Language (SQL) opfattet som standard sproget til relationelle databaser ?
    4. Hvilken faktor var afgørende for database industriens store vækst i 1990'erne ?
    5. Hvilke typer af databaser findes der ?
    6. Hvornår begyndte 'NoSQL' databaser at dukke op ?
2. Vi afslutter med "30 til buffet" på klassen

## Links

Alle ressourcer kan findes i dagens plan på itslearning