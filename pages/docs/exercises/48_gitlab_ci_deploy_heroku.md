 

# Opgave 48 - heroku/gitlab CI/CD deploy

## Information

I denne opgave kan du lære hvordan din api udgives på [https://www.heroku.com/](https://www.heroku.com/)

heroku er en cloud platform som er meget simpel at bruge. Indtil den 28. November 2022 er den gratis at bruge for alle, men efter den 28. koster det penge. 
Dog ikke for studerende (yay) der ansøger om github student developer pack [https://education.github.com/pack/join](https://education.github.com/pack/join)

Der er flere måder at udgive på heroku, jeg vil instruere i hvordan det oftest foregår i software virksomheder, med continuous integration, continuous delivery, and continuous deployment (CI/CD).

CI/CD er automatisering af bygge og udgivelses processen af din applikation.  
Det du gør manuelt nu hvor du bygger og starter din applikation med `node app.js` bliver automatiseret i skyen.  
Udover det kopieres ændringer også til heroku automatisk.  

CI/CD kan bruges til rigtig meget men er grundlæggende en serie shell kommandoer som bliver afviklet i en eller flere containere (docker er populært!)  
Når kommandoerne er afviklet lukkes containeren igen.  

Der er adskillige cloud platforme der tilbyder CI/CD:

- Github
- Harness
- Jenkins
- Azure Devops
- Google cloud
- Gitlab
- og sikkert mange flere

Jeg er mest hjemme i Gitlab og derfor bruger jeg det til eksemplet.  
Mit indtryk er at der ikke er stor forskel på hvordan de forskellige platforme bruger CI/CD og derfor burde du kunne overføre din viden om konceptet til andre platforme hvis du møder dem senere i din karriere.

Når din api er udgivet på Heroku er den tilgængelig for alle på internettet.  

Hvis du af en eller anden grund ikke har lyst til at lære CI/CD flowet, kan man bruge heroku CLI, men det er væsentligt mere omstændigt end CI/CD  
[https://devcenter.heroku.com/articles/getting-started-with-nodejs?singlepage=true](https://devcenter.heroku.com/articles/getting-started-with-nodejs?singlepage=true)

**Husk at læse hele opgaven inden du går i gang med at arbejde!**

Hvis du går i stå kan du se hvordan jeg har gjort på [https://gitlab.com/ucl-pba-wu/node-mongo-todo/-/tree/013285bfac3e5faf5eae2c6f3c1aef413ea4df79](https://gitlab.com/ucl-pba-wu/node-mongo-todo/-/tree/013285bfac3e5faf5eae2c6f3c1aef413ea4df79)

## Instruktioner

**Del 1: todo app filer på gitlab med GIT**

1. Lav en fil der hedder `.gitignore` i todo applikationen og skriv følgende 2 linjer i filen
```
node_modules/
.env
```
2. Lav en heroku konto hvis du ikke allerede har en [https://signup.heroku.com/](https://signup.heroku.com/)
3. Lav en gitlab konto hvis du ikke allerede har en [https://gitlab.com/users/sign_up/](https://gitlab.com/users/sign_up/) du kan bruge din heroku konto her (salesforce) eller github til at oprette en konto på gitlab.
4. Lav et nyt gitlab projekt og klon det til din computer ved at skrive `git clone *repo-url*` i en terminal (git bash, terminal, cmd etc. powershell virker vist ikke med git)  
Jeg lavet en lille guide her [https://ucl-pba-its.gitlab.io/exercises/intro/2_intro_opgave_git_gitlab_ssh/](https://ucl-pba-its.gitlab.io/exercises/intro/2_intro_opgave_git_gitlab_ssh/). Guiden linker også til en anden guide hvis du vil sætte gitlab op til at bruge `ssh`
5. Kopier alle filerne der hører til din todo-app over i den klonede gitlab mappe
6. Tilføj de flyttede filer til git med kommandoen `git add .`
7. Commit ændringer til git med kommandoen `git commit -am "dit beskrivende commit message"`
8. Push ændringer til gitlab.com med kommandoen `git push`

**Del 2: npm start scripts**

1. Åbn `package.json` og indsæt følgende i script sektionen
```json
"scripts": {
    "start": "node --use_strict ./app.js",
    "test": "nodemon ./app.js",
    "start-gendoc": "node swagger.js"
  },
```
2. Test at din applikation kan startes med kommandoen `npm run start`
3. Test at din applikation kan startes med kommandoen `npm run test`

`start` scriptet er nødvendigt til heroku og anvendes hver gang heroku app'en startes  
`test` er til brug når du udvikler på todo applikationen lokalt  
`start-gendoc` er til generering af swagger dokumentation, det kommer du til i opgave **Opgave 49 - swagger API dokumentation**  

**Del 3: Heroku app opsætning**

1. Log ind på heroku [https://heroku.com/](https://heroku.com/)
2. Opret en ny app på Heroku  
![images/heroku_new_app.png](images/heroku_new_app.png)  
3. App’en skal havde et navn som er unikt således det ikke overlapper med andre applikationer på Heroku. Som region skal Europa vælges.  
Gem app navnet i en tekst fil til senere brug  
![images/heroku_app_name.png](images/heroku_app_name.png)  
4. Når app’en er oprettet, skal du bruge den Api nøgle som hører til din heroku konto.  
Nøglen bruges til autentifikation, således at udvekommende ikke kan lave et deployment med din heroku konto.  
Gitlab benytter api nøglen til at autentificere sig når der skal laves et nyt deployment fra kildekoden på Gitlab. 
For at få api nøglen skal du ind i Account settings på heroku  
![images/heroku_account_settings.png](images/heroku_account_settings.png)  
Scroll ned til API Key og kopier nøglen (reveal den først)  
![images/heroku_api_key.png](images/heroku_api_key.png)
5. Senere i gitlab skal api nøglen bruges, derfor kan det være en fordel at kopier nøglen over i et midlertidigt tekst dokument eller password manager.

**Del 4: MongoDB Atlas miljøvariabel** 

1. Kopier din connection string fra `.env` filen
2. Gå til din heroku app og klik på **settings**, scroll ned til **config vars** og klik på **reveal config vars**
3. Lav en config var hvor key er `ATLAS_CON_STRING` og value er din connection string  
![images/heroku_config_vars.png](images/heroku_config_vars.png)

**Del 5: Gitlab CI/CD variabler**

1. Gå til dit gitlab projekt og vælg CI/CD under projekts opsætning.  
![images/gitlab_cicd_settings.png](images/gitlab_cicd_settings.png)
2. Inde i CI/CD opsætningen skal der sættes to variabler.  
En som indeholder værdien af API nøglen fra tidligere, og en som indeholder det navn som du gav din app på Heroku.
3. Scroll til variables og expand
![images/gitlab_variables_settings.png](images/gitlab_variables_settings.png)
4. Opret en variabel hvor key er `HEROKU_API_KEY` og value er heroku API nøglen
![images/gitlab_heroku_api_key.png](images/gitlab_heroku_api_key.png)
5. Opret en anden variabel hvor key er `HEROKU_APP_NAME` og value er navnet på din heroku app 

**Del 6: Gitlab CI/CD konfigurations fil**

1. I VS Code, lav en fil der skal hedde `.gitlab-ci.yml` og indsæt følgende i filen
    ```yml
    stages: 
    - production

    heroku_deploy:
    stage: production
    image: ruby:latest
    script: 
      - gem install dpl
      - dpl --provider=heroku --app=$HEROKU_APP_NAME --api-key=$HEROKU_API_KEY
    ```  
    Her er det værd at bemærke at værdien af app og api-key tildeles fra variablerne $HEROKU_APP_NAME og $HEROKU_API_KEY.  
    Det er de to variabler som blev oprettet i gitlab projektets CI/CD opsætning.  
    Yderlige information om ci konfigurations filen kan findes [https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)  
2. Tilføj `.gitlab-ci.yml` git med kommandoen `git add .`  
3. Commit ændringer til git med kommandoen `git commit -am "dit beskrivende commit message"`
4. Push ændringer til gitlab.com med kommandoen `git push`
5. gå til dit gitlab projekt, udvid ci/cd og klik på pipelines
![images/gitlab_pipeline.png](images/gitlab_pipeline.png)
6. Klik på seneste pipeine status som kan være running, passed eller failed og derefter det aktuelle job
![images/gitlab_pipeline_running.png](images/gitlab_pipeline_running.png)
7. Kig outputtet fra jobbet igennem og se hvad der sker, det burde være stort set det samme som når du krer node applikationen lokalt på din computer + deployment til heroku.  
Hvis alt er gået godt slutter jobbet med `Job succeeded` og din api kan findes på adressen for din heroku app  
![images/gitlab_job_output.png](images/gitlab_job_output.png)  
Hvis ikke så har du gjort noget forkert og skal i gang med at fejlfinde :-)
8. Hvis alt er godt og din API er deployet på heroku, bør du nu teste den med postman og den nye URI på heroku

## Links
