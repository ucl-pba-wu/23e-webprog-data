 

# Opgave 30 - Viden om IT sikkerhed 

## Information

Sikkerhed er et stort emne og er mere aktuelt end nogensinde.  

Dels er der en del lovgivning på området som [GDPR](https://eur-lex.europa.eu/legal-content/DA/TXT/HTML/?uri=CELEX:02016R0679-20160504) og der er meget mere på vej som [NIS2](https://eur-lex.europa.eu/legal-content/DA/TXT/HTML/?uri=CELEX:32022L2555&qid=1699546364855) og [Cyber resiliance act](https://eur-lex.europa.eu/legal-content/DA/TXT/HTML/?uri=CELEX:52022PC0454) der omhandler sikkerhed i hhv. kritisk infrastruktur og digitale produkter. Dette forventes at få samme udbredelse som GDPR hvilket betyder at IT sikkerhed bliver almindelig viden for alle og alle der arbejder med udvikling af digitale produkter skal have kompetencer indenfor IT sikkerhed.  

Årsagen er at daglige angreb på danske og udenlandske virksomheder og at cyberkriminalitet er den type kriminalitet der målt på [indtjening](https://www.statista.com/forecasts/1280009/cost-cybercrime-worldwide) er absolut størst.  

Motivationen hos angribere kan være mange, fra aktivisme (anonymous) til statsfinansierede grupper (Lazarus/Nord korea, REvil/Rusland).  

De fleste angreb udføres dog af kriminelle med profit som motivation.      

## Ransomware

Et af de helt store emner er pt. ransomware angreb.  

Kort fortalt er ransomware et begreb der dækker over at en virksomheds data bliver stjålet og samtidig krypteret(låst) så virksomheden ikke har adgang til data mere.  

Data er som regel følsom information om kunder og ansatte, forretningshemmeligheder, brugernavne, passwords, mails, regnskaber og andet der kan skade virksomheden hvis de bliver offentliggjort.  

De kriminelle kræver derefter en ransom (løsesum) for at sende nøglen der kan dekryptere data. De lover også at data ikke bliver offentliggjort hvis løsesummen betales (det er ikke altid de overholder det)  

IT kriminalitet er i dag business as usual forstået på den måde at professionelle it kriminelle er organiseret som enhver anden virksomhed med forskellige afdelinger.  
Produktet de sælger er Ransomware As A Service (Raas) som dels er noget "kunder" udefra kan bestille,det vil sige at de kan købe sig til et angreb.  
Men ofte fremstiller grupperne sig som "sikkerhedstestere" der udfører en "service" for de virksomheder de angriber, argumentet er at de hjælper virksomhederne med at teste deres sikkerhed, godt nok uden at virksomheden har bedt om det..... 

Du kan læse mere om ransomware truslen i denne microsoft artikel [https://www.microsoft.com/security/blog/2022/05/09/ransomware-as-a-service-understanding-the-cybercrime-gig-economy-and-how-to-protect-yourself/](https://www.microsoft.com/security/blog/2022/05/09/ransomware-as-a-service-understanding-the-cybercrime-gig-economy-and-how-to-protect-yourself/)  

## Hvordan foregår et angreb ?

Kriminelle har brug for en åbning i en virksomheds systemer for at kunne opnå adgang.  
Når de har fundet en åbning udnytter de den til at opnå administrator/root rettigheder og kan derefter gøre hvad de vil på systemet.  

Åbninger skyldes ofte forældet software med kendte eller ukendte sikkerheds huller.  
Ukendte sikkerheds huller kaldes også for *0day*, fordi der er 0 dage til at rette sårbarheden.   
Software er alt fra Server software, firmware i netværksudstyr og applikationer virksomheden selv udvikler.  

Kendte sikkerhedshuller offentligøres som det der hedder *Common Vulnerabilities and Exposures*(CVE) [https://www.redhat.com/en/topics/security/what-is-cve](https://www.redhat.com/en/topics/security/what-is-cve)  

Der findes adskillige CVE databaser, en af dem vedligeholdes af den amerikanske organisation NIST(National Institute of Standards and Technology).  

Her kan du f.eks finde en SQL server sårbarhed fra 2022 der giver en angriber mulighed for at eksekvere kode på SQL Serveren [https://nvd.nist.gov/vuln/detail/CVE-2022-29143](https://nvd.nist.gov/vuln/detail/CVE-2022-29143)  

Det er almindeligt at sikkerheds researchere laver det der hedder et proof of concept(POC) på kendte sårbarheder, POC er ofte et stykke kode(exploit) der kan udnytte sårbarheden.
Der findes websider med databaser over exploits som f.eks [https://www.exploit-db.com](https://www.exploit-db.com)

Et angreb foregår i grove træk efter nedenstående punkter [Mandiant targeted attack lifecycle](https://www.mandiant.com/resources/insights/targeted-attack-lifecycle):

1. **Initial Reconnaissance**  
  Rekognosering af målet (offentlig tilgængelig data) og kortlægning af de teknologier målet anvender samt søgning efter sårbarheder hos målet inkl. phishing mål.
2. **Initial Compromise** Adgang til system hos målet.
3. **Establish Foothold** Mere stabil adgang adgang til systemer ved hjælp af bagdøre eller specialiceret angrebssoftware etc. 
4. **Escalate Privileges** Udnyttelse af brugere og systemer der har flere rettigheder, målet er at få rettigheder til alt, såkaldt root access (administrator rettigheder) på alle målets systemer.
5. **Internal Reconnaissance** Rekognosering, denne gang på indersiden af målets systemer, dette gøres for at identificere infrastruktur som servere, routere og andre systemer der kan prøevs at opnå adgang til.
6. **Move Laterally** Opnå adgang til andre dele målets systemer ved bevægelse gennem remote desktop sessioner, netværks drev etc.
7. **Maintain Presence** Varig adgang til systemer ved hjælp af flere bagdøre og gerne mere stabile forbindelser som VPN adgang der er krypterede og skjuler angriberens akiviteter
8. **Complete Mission** Tyveri af værdifulde data og kryptering   så målet ikke kan tilgå egne data. Det kan også være at ødelægge data og systemer hvilket er set i Ukraine krigen. Her foregår også oprydning i logs og andre steder der kan afsløre angriberens aktivitet (post exploitation)

For hvert punkt er der selvfølgelig en hel verden af begreber og teknologier som det desværre ikke er muligt at dække i denne lille gennemgang.  

## Lovgivning og standarder i IT sikkerhed

Lovgivning og standardisering er som nævnt tidligere en stor del at it-sikkerhed.  

Lovgivning skal virksomheder leve op til, standardisering er et værktøj og et kvalitetsstempel flere, især større virksomheder, benytter sig af og også bruger som salgsargument overfor deres kunder så de ved at det er sikkert at handle med virksomheden.

Standarder kan hjælpe med at overholde lovgivning og derfor kan det være en fordel for virksomheder at implemetere standarder.
Overholdelsen af standarder kan også være et krav fra samarbejdspartnere, eller et krav hvis virksomheden arbejder med det offentlige.

Som webudvikler kan du komme til at arbejde med opgaver der skal overholde GDPR lovgivningen, det kan være du arbejder med en database der indeholder personfølsom information eller du implementerer funktionalitet i en webapplikation så brugere kan få en kopi af gemt person data (ret til dataportabilitet) og mulighed for at slette data (retten til at blive glemt).

### GDPR

GDRP står for *General Data Protection Regulation* og gælder for alle virksomheder der opererer i EU.  

Lovgivningen er lavet for at beskytte os alle mod misbrug af vores personlige data.  
Konsekvensen for en virksomhed der ikke efterlever GDPR kan være stor økonomisk i form af bøder, men også i forhold til virksomhedens omdømme.  

### DS/ISO27001

I europa er DS/ISO27001 standarden der benyttes når virksomheder vil implementere og vedligeholde deres sikkerhed. 
DS/ISO27001 kan være omfattende for små virksomheder at implementere. Derfor er det dog stadig vigtigt at de beskytter sig i en rimelig grad (hvilket mange desværre ikke gør)  
DS/ISO27001 dokumenterne har du adgang til via Dansk Standard Distribute og din UCL studie konto [https://sd.ds.dk/Account/LogOn](https://sd.ds.dk/Account/LogOn) 

### D-Mærket

D-Mærket er en mærkningsordningen på samme måde som e-mærket er det.  

D-Mærket er målrettet små- og mellemstore virksomheder og kan hjælpe med at sætte 
fokus på deres sikkerhedsniveau gennem blandt andet en selv evaluerings test.  

D-Mærket har fået kritik i it-sikkerhedsbranchen fordi det kan give en falsk følelse af sikkerhed hvis ikke virksomhedens sikkerhed evalueres og opdateres jævnligt.  
På trods af kritikken, synes jeg, at det er en god start og bedre end ingenting. 

### OWASP Application Security Verification Standard

OWASP har lavet en standard til web applikationer der hedder Application Security Verification Standard som beskriver hvordan sikkerhed kan implementeres i udviklingen af (blandt andet) web applikationer [https://owasp.org/www-project-application-security-verification-standard/](https://owasp.org/www-project-application-security-verification-standard/)  
Standarden er gratis (open source) og er et af de bedste værktøjer til sikring af applikationer.

### Privacy & security by design & default

*Privacy by design (PbD) indebærer, at virksomheden tænker og bygger databeskyttelses- og privatlivsrelaterede værn ind i produkter og services fra udviklingens tidligste stadier og igennem hele livscyklussen.*

*Grundprincippet i security by design (SbD) er, at sikkerhed tænkes og bygges ind i produkter og tjenester fra udviklingens tidligste stadier og igennem hele livscyklussen.*  
*[Digital Tryghed - 2022](https://d-maerket.dk/kriterier/privacy-security-by-design-default/)*

Det er meget sandsynligt at du vil skulle arbejde med disse 2 begreber i din karriere og det er derfor godt at kende til dem.

## Instruktioner

Opgaverne herunder skal i lave sammen i jeres gruppe.  
Når i arbejder så skriv jeres fælles svar ned i et dokument.  
Som afrunding på opgaven debatterer vi jeres besvarelser på klassen.

1. Læs og diskuter indholdet i [GDPR](https://gdpr.dk/databeskyttelsesforordningen/) og hvilke krav virksomheder skal efterleve    
  **Spørgsmål:** 
    - Hvad er GDPR?
    - Skal i som webudviklere overholde GDPR i de applikationer i udvikler?
    - Hvilke dele af GDPR mener i er vigtigt at overholde i en webapplikation der behandler personfølsom data?
    - Hvordan forestiller i jer at en applikation kan understøtte at brugere selv kan administrere der personlige data, herunder retten til at blive glemt (artikel 17) og ret til dataportabilitet (artikel 20)?

2. Læs om [ISO27001 (dansk standard)](https://www.ds.dk/da/om-standarder/ledelsesstandarder/iso-27001-informationssikkerhed)  
  Hent ISO27001, ISO27002 og ISO27005 fra [https://sd.ds.dk/Account/LogOn](https://sd.ds.dk/Account/LogOn) og gem dem på din computer  
  **Spørgsmål (Svar kan findes i standarderne og via dansk standard linket):**
    - Hvad er hhv. ISO27001, ISO27002 og ISO27005?
    - Hvordan kan standarderne være relevante for jer som webudviklere?  
    - Hvordan forestiller i jer at jeres kommende arbejdsplads arbejder med ISO 27001?
    - Kan ISO 27001 have forretningsmæssig værdi og hvis ja, hvordan?
    - Er der et lovkrav om at overholde ISO27001?
  
3. Læs om [D-Mærkets kriterier](https://d-maerket.dk/kriterier/)
  **Spørgsmål:**
    - Hvad er D-Mærket?
    - Hvor mange kriterier er der i D-Mærket?
    - Hvad er Privacy & security by design & default og har det noget med udvikling af web applikationer at gøre?
    - Skal en virksomhed leve op til alle kriterier for at få et D-Mærke?
    - Kan D-Mærket have forretningsmæssig værdi og hvis ja, hvordan?
    - Find virksomheder på nettet som har et D-mærke. Hvilken information får du ved at klikke på deres D-Mærke? (gem virksomheders navn og links) 

4. Læs i OWASP [Application Security Verification Standard (ASVS)](https://github.com/OWASP/ASVS/raw/v4.0.3/4.0/OWASP%20Application%20Security%20Verification%20Standard%204.0.3-en.pdf) og skab et overblik over dokumentets indhold.  
  **Spørgsmål:**
    - Hvad er ASVS ?
    - Kan en virksomhed blive certificeret i ASVS?
    - Hvad kan man finde i de forskellige kontroller ? (V1 - V14) - beskriv med egne ord
    - Skriv 3 eksempler på kontroller i finder interessante i forhold til webudvikling. 
    - Hvordan forestiller i jer at ASVS kan støtte udvikling af sikre web applikationer ?

## Links

Hvis du vil læse mere om it sikkerhed kan denne bog anbefales  
- [https://it-sikkerhedsbogen.dk/](https://it-sikkerhedsbogen.dk/)

Der er gode vejledninger og information om sikkerhed hos Center For Cyber Sikkerhed

- [https://www.cfcs.dk/da/forebyggelse/vejledninger/](https://www.cfcs.dk/da/forebyggelse/vejledninger/)

