 

# Opgave 37 - MongoDB update og delete

## Information

I sidste opgave linkede jeg til MongoDB dokumentationen for update

### UPDATE

- updateOne [https://docs.mongodb.com/manual/reference/method/db.collection.updateOne/](https://docs.mongodb.com/manual/reference/method/db.collection.updateOne/)
- updateMany [https://docs.mongodb.com/manual/reference/method/db.collection.updateMany/](https://docs.mongodb.com/manual/reference/method/db.collection.updateMany/)
- replaceOne [https://docs.mongodb.com/manual/reference/method/db.collection.replaceOne/](https://docs.mongodb.com/manual/reference/method/db.collection.replaceOne/)

Basis syntaks for updateMany ser sådan her ud

```js
db.collection.updateMany( filter, update, options )
```

og et eksempel på brug kan ses her:

![images/mongo_updatemany.png](images/mongo_updatemany.png)

Jeg linkede også til dokumentation for delete

### DELETE

- deleteOne [https://docs.mongodb.com/manual/reference/method/db.collection.deleteOne/#db.collection.deleteOne](https://docs.mongodb.com/manual/reference/method/db.collection.deleteOne/#db.collection.deleteOne)
- deleteMany [https://docs.mongodb.com/manual/reference/method/db.collection.deleteMany/#db.collection.deleteMany](https://docs.mongodb.com/manual/reference/method/db.collection.deleteMany/#db.collection.deleteMany)

Basis syntaks for updateMany ser sådan her ud

```js
db.collection.deleteMany(filter, options)
```

og et eksempel på brug kan ses her:

![images/mongo_deletemany.png](images/mongo_deletemany.png)

## Instruktioner

Lav følgende i classic cars databasen.  
Check dine ændringer i databasen for hver af de nedenstående opgaver.
Kommandoerne skal eksekveres i *mongosh* som du installerede i **Opgave 35 - MongoDB Server og værktøjer**

1. Dokumenter dine kommandoer i en fil der hedder **classic_cars_update_delete_solution.js**
2. Indsæt 3 nye clients i *classic cars* databasen, de skal alle have efternavnet *Deleteme* 
3. Opdater en valgfri clients biler til `{"Model": "Skoda", "Year": "2013", "Value": "50000"}`
4. Opdater alle clients der starter med b, så deres by er *Odense*
5. Slet alle clients med efternavnet Deleteme 

## Links

Hvis du ikke fik lavet classic cars databasen i opgave 36 er der lidt hjælp her:

```js
use classic_cars

db.client.insertMany([
{"Pers_ID": "0", "Surname": "Miller", "First_Name": "Paul", "City": "London", "Cars":[{"Model": "Bently", "Year": "1973", "Value": "100000"}, {"Model":"Rolls Royce", "Year": "1965", "Value": "330000"}]},
{"Pers_ID": "1", "Surname": "Ortega", "First_Name": "Alvaro", "City": "Valencia", "Cars":[{"Model": "", "Year": "", "Value": ""}]},
{"Pers_ID": "2", "Surname": "Huber", "First_Name": "Urs", "City": "Zurich", "Cars":[{"Model": "Smart", "Year": "1999", "Value": "2000"}]},
{"Pers_ID": "3", "Surname": "Blanc", "First_Name": "Gaston", "City": "Paris", "Cars":[{"Model": "Renault", "Year": "1998", "Value": "2000"}, {"Model":"Renault", "Year": "2001", "Value": "7000"}]},
{"Pers_ID": "4", "Surname": "Bertolini", "First_Name": "Fabrizio", "City": "Rome", "Cars":[{"Model": "Ferrari", "Year": "2005", "Value": "150000"}]}
])
```
