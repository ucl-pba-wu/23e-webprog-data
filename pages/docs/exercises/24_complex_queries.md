 

# Opgave 24 - Komplekse forespørgsler

## Information

Dette er en opgave der bygger videre på **Opgave 23 - Bike stores database** og giver mulighed for at træne forespørgsler hvor både `JOIN`, `GROUP BY` og SQL funktioner indgår.

Forespørgslerne skal udføre på `BikeStores` databasen

![images/bike_stores_schema.png](images/bike_stores_schema.png)

## Instruktioner

Lav følgende forespørgsler:

1. Hvad er prisen på det dyreste produkt ? Rund op til nærmeste heltal.  

    _resultat:_  
  
    ||MostExpensiveProduct|
    |---|---|
    |1|12000|

2. Vis kundernes firstname, lastname og initialer. F.eks. Debra Burks (DB)  

    _resultat (kun vist 2 første + 2 sidste linjer):_  

    || first_name |	last_name |	Initials |
    |--- | ---| --- |--- |
    |1| Debra	| Burks	| (DB) |
    |2| Kasha	| Todd	| (KT) |
    |...|...|...|...|
    |1444| Ivette	| Estes	| (IE) |
    |1445| Ester	| Acevedo	| (EA) |
    
3. Vis antal produkter pr. brand_id. Vis kun brands der har mere end 100 produkter  

    _resultat:_  
  
    ||brand_id|productsPerCategory|
    ||---|---|
    |1|1	| 118| 
    |2|9	| 135| 

4. Vis antal produkter pr. brand_name og antal produkter pr. category id. Vis kun kategorier der har mere end 50 produkter  

    _resultat:_  
  
    ||brand_name|productsPerCategory|
    ||---|---|
    |1|Electra	| 118| 
    |2|Trek	| 135| 

5. Hvor mange ordrer er oprettet af den ansatte der hedder Mireya Copeland ?  

    _resultat:_  
  
    ||numOforders|
    |---|---|
    |1	| 164| 

6. Vis alle butiksnavne og hvor mange produkter de hver har på lager  

    _resultat:_  

    || store_name |	TotalProductsPerStore |	
    |--- | ---| --- |
    |1| Baldwin Bikes	| 4359	| 
    |2| Rowlett Bikes	| 4620	| 
    |3| Santa Cruz Bikes	| 4532	| 

7. Vis alle i produkter i kategorien 'Comfort Bicycles'. Vis kun produkt navn, sorter alfabetisk.  

    _resultat (kun vist 2 første + 2 sidste linjer):_  
  
    ||product_name|
    |---|---|
    |1	| Electra Townie Balloon 3i EQ - 2017/2018 | 
    |2	| Electra Townie Balloon 3i EQ Ladies' - 2018 | 
    |...|...|
    |29	| Sun Bicycles Streamway 3 - 2017| 
    |30	| Sun Bicycles Streamway 7 - 2017| 


## Links
