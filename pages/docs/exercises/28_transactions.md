 

# Opgave 28 - SQL transactions

## Information

SQL transaktioner er en enhed der samler en eller flere forespørgsler. Transaktioner sikrer konsistens i forespørgsler og har også indbygget mekanismer til at forhindre ændringer i databasen hvis transaktionen mislykkes.

Herunder er et eksempel på en transaktion

```SQL
BEGIN TRY
  BEGIN TRANSACTION;

  DECLARE @neworderid AS INT;

  -- Insert a new order into the Orders table
  INSERT INTO Orders (...)VALUES (...);

  -- Save the new order ID in a variable
  SET @neworderid = SCOPE_IDENTITY();

  -- Insert order lines for the new order into OrderDetails
  INSERT INTO OrderDetails (...)
    VALUES(@neworderid, 11, 14.00, 12, 0.000),
          (@neworderid, 42, 9.80, 10, 0.000);

  COMMIT TRANSACTION;
END TRY
BEGIN CATCH
  ROLLBACK TRANSACTION;
END CATCH;
```

I eksemplet er et par nye ting du måske ikke har set før.

`BEGIN TRY, END TRY, BEGIN CATCH, END CATCH` tilhører en construktion der i daglige tale omtale som en try/catch.  
Den består af 2 blokke, en try blok og en catch blok. Hver af blokkene har en begin og end.  
En try catch tester den kode der er skrevet i try blokken, hvis noget der fejler vil koden i catch blokken blive afviklet, hvis der ikke er fejl ignoreres catch blokken.  

Dokumentation for try/catch er her: [https://learn.microsoft.com/en-us/sql/t-sql/language-elements/try-catch-transact-sql](https://learn.microsoft.com/en-us/sql/t-sql/language-elements/try-catch-transact-sql)

I dette eksempel er der i try blokken defineret en transaktion med `BEGIN TRANSACTION` som efterfølges af nogle `INSERT` statements.  
Efter `INSERT` bruges `COMMIT TRANSACTION` til at udføre transaktionen, det vil sige afvikle det der er defineret i transaktionen.  
Det er selvfølgelig muligt selv at definere hvad der skal udføre i transaktionen.

Hvis transaktionen fejler vil koden i catch blokken blive afviklet som i dette tilfælde ruller transaktionen tilbage. Det vil sige at der ikke bliver ændret noget i databasen.  
På den måde kan transaktioner sikre at der ikke bliver lavet fejl når der skrives i databasen.

Transaktioner overholder **A**tomicity, **C**onsistency, **I**solation og **D**urable (ACID) princippet.

Dokumentationen for transaktioner er her: [https://learn.microsoft.com/en-us/sql/t-sql/language-elements/transactions-transact-sql](https://learn.microsoft.com/en-us/sql/t-sql/language-elements/transactions-transact-sql)

## Instruktioner

1. læs om transaktioner. Når du læser så læg mærke til syntaksen for opbygning af en transaktion, formålet med transaktioner og detaljerne omkring ACID princippet [https://www.sqlshack.com/transactions-in-sql-server-for-beginners/](https://www.sqlshack.com/transactions-in-sql-server-for-beginners/) 
2. Opret databasen many_test som eksemplet i **Opgave 13 - many to many relationer (M:N)** bruger. Husk også at putte data i databasen (også inkluderet i eksemplet).
3. Skriv en transaktion der opretter en ny bog og tilføjer en eksisterende forfatter til bogen. Hvis noget i transaktionen går galt, f.eks at forfatteren du tilføjer til bogen ikke eksisterer skal transaktionen ikke gennemføres.
4. Test din transaktion med en forfatter der ikke eksisterer og kontroller at bogen ikke blev oprettet.
5. Test din transaktion med en forfatter der eksisterer og kontroller at bogen blev oprettet.

## Links
