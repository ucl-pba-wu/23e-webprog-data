 

# Opgave 31 - Sikkerhed i MS SQL Server 

## Information

I [Opgave 30](../exercises/30_IT_Sikkerhed.md) har du fået et lille indblik i hvad du risikerer hvis du ikke sikrer dine web applikationer og bagvedliggende systemer og derfor har du nok lyst til at vide lidt om hvordan du kan sikre dig?  

Det er dog vigtigt at bemærke at du aldrig kan blive 100% sikker fordi der løbende bliver opdaget nye sikkerheds huller og nye teknikker til at bryde eksisterende sikkerhed.  
Sikkerhed er en proces som løbende skal holdes ved lige, også af webudviklere. 

## Sikring af databaser

SQL server som vi pt. arbejder med implemeterer sikkerhed på flere niveauer:

- Platformen som serveren kører på (server HW og SW)
- Authentication (bevis for at du er den du udgiver dig for at være)
- Authorization (hvad har applikationen/brugeren lov til at gøre)

**Platform**  
er den fysiske hardware som databasen er installeret på og de netværkssystemer der forbinder til hardware og ultimativt databasen. 

**Authentication**  
handler om at brugere (mennesker og applikationer) skal bevise hvem de er for at få adgang til hhv. database systemet (master databasen: login og DB users) samt de enkelte databaser der kører på serveren.  
SQL server benytter 2 former for authentication, Windows authentication og SQL Server Authentication.  

**Authorization**  
Er styring af hvad den enkelte brugergruppe/bruger/applikation har lov til at gøre i databasen. Dette gælder for eksempel styring af adgang til både databser og databasernes indhold helt ned på kolonne niveau. 

### Roller

Roller beskriver hvem du er og hvad du kan i en given kontekst, du kender sikkert f.eks administrator rollen og bruger rollen.

SQL server anvender 2 sæt roller, server roller og database roller.

Roller skal forstås som en gruppe en given bruger/applikation kan være medlem af.   
Hvis brugeren/applikationen er medlem af rolle gruppen opnås rollens privilegier. 

Oprettelse af brugere og tildeling af roller kan ske grafisk i SSMS eller via SQL kommandoer.

**Server roller**

SQL server har et antal foruddefinerede roller som dækker forskellige niveauer (afhængig af SQL server versionen) af rettigheder til SQL serveren softwaren.

Du kan se et overblik over rollerne i denne artikel [https://learn.microsoft.com/en-us/sql/relational-databases/security/authentication-access/server-level-roles](https://learn.microsoft.com/en-us/sql/relational-databases/security/authentication-access/server-level-roles)  

Rollerne i SQL server er designet ud fra *Principle of least priviledge(POLP)* som betyder at der kun skal gives adgang til den information og de ressourcer der er nødvendige for at udføre en given opgave.  

Ved at anvende POLP begrænses adgang for en eventuelt angriber, ved at angriberen kun får adgang til et begrænset sæt privilegier, f.eks kun at kunne læse data men ikke slette det.  

POLP er en overordnet sikkerheds strategi, der anvendes overalt hvor der anvendes brugerstyring. 

Du kan læse mere om POLP i SQL server kontekst her  
[https://techcommunity.microsoft.com/t5/azure-sql-blog/security-the-principle-of-least-privilege-polp/ba-p/2067390](https://techcommunity.microsoft.com/t5/azure-sql-blog/security-the-principle-of-least-privilege-polp/ba-p/2067390)  

Det er muligt at lave sine egne server roller, de kaldes *user-defined server level roles*

**Database roller**

Database roller dækker over et antal forudefinerede roller til at styre adgang til de enkelte databaser.
Den enkelte rolle beskriver hvad der gives adgang til. 

For eksempel står der om *db_datawriter:*
```
Members of the db_datawriter fixed database role can add, delete, or change data in all user tables.
```

Læs mere om database roller her:  
[https://learn.microsoft.com/en-us/sql/relational-databases/security/authentication-access/database-level-roles](https://learn.microsoft.com/en-us/sql/relational-databases/security/authentication-access/database-level-roles)

### Eksempler på anvendelse af sikkerhed på SQL server

### Oprettelse af login til MS SQL server

Eksemplet opretter brugeren student01 og tildeler brugeren rollen *db_creator*.
student01 kan således oprette databser på serveren, men har ikke adgang til eksisterende databaser

**SQL**
```SQL
USE master
GO

CREATE LOGIN student01 WITH PASSWORD=N'Aa12341234!'
GO

ALTER SERVER ROLE dbcreator ADD MEMBER student01
GO
```

**Grafisk SSMS**
![images/sec_create_login.png](images/sec_create_login.png)

### Adgang til specifik database

Login giver adgang til serveren, en USER giver adgang til en specifik database.  
Et login kan associeres med mange users. 

```SQL
USE world
GO

CREATE USER student01 FOR LOGIN student01
GO

ALTER ROLE db_datareader ADD MEMBER student01
GO
```

**Grafisk SSMS**
![images/sec_db_access.png](images/sec_db_access.png)

### Adgang til database objekter

Nedenstående fjerner student01 fra db_datareader gruppen og efterfølgende gives adgang til en stored procedure der hedder `spGetCitiesPerCountry`

```SQL
USE [World]
GO

ALTER ROLE [db_datareader] 
DROP MEMBER [student01]
GO

GRANT EXECUTE ON 
[dbo].[spGetCitiesPerCountry] TO [student01]
GO

```

**Grafisk SSMS**  
![images/sec_sp_access.png](images/sec_sp_access.png)

## Instruktioner

Nu er det din tur til at arbejde med database sikkerhed i Microsofts SQL Server :-)

Hvis du er på Windows skal du først sikre er at din SQL server er sat op til at acceptere `SQL Server and windows Authentication mode`  

Dette kan du kontrollere i både Microsoft server managament studio eller azure data studio.  

Disse instruktioner viser det i Azure data studio. 

Jeg er ret sikker på at det ikke er nødvendigt på mac/docker og kan ikke teste det (jeg er på windows), men kontroller et og tag fat i din underviser hvis der er indstillinger som ikke er `SQL Server Authentication mode`

1. Åbn Azure data studio og åbn din database forbindelse
2. Højreklik på din database forbindelse og vælg properties
  ![](images/db_security_settings_edit.png)
3. I security vælg `SQL Server and windows Authentication mode` bekræft med apply knappen  
  ![](images/db_security_settings.png)
4. For at ændringerne virker skal du genstarte din sql server, dette gøres i en applikation vi ikke har anvendt før men som burde være installeret, applikationen hedder [SQL Server Configuration Manager](https://learn.microsoft.com/en-us/sql/database-engine/configure-windows/start-stop-pause-resume-restart-sql-server-services?view=sql-server-ver16) og startes fra windows Start menu:  
`alle programmer > Microsoft SQL Server > Configuration Tools > SQL Server Configuration Manager` (lidt afhængig af den version du har installeret)
  ![](images/db_security_settings_sql_server_manager.png)
5. Genstart serveren og vent på at den bliver færdig
  ![](images/db_security_settings_sql_server_manager_restart.png)

### Brugere og rettigheder i MS SQL

Du er nu klar til at oprette brugere og styre rettigheder.  

Der findes som sagt både brugere/adgange til database serveren (login) samt til de enkelte databaser (user).  
Database server login tildeles rettigheder der bestemmer hvad der kan udføres på serveren som oprettelse af databaser, oprettelse af brugere etc.

Database brugere (user) tildeles rettigheder der bestemmer hvad de må i den enkelte database. Oprettelse af nye tabeller, kun læsning, kun adgang til bestemte views eller stored procedures etc.

I denne øvelse knyttes et database bruger, der har rettigheder i world databasen, til et database server login der ikke har nogle rettigheder på serveren. På den måde kan der opnås adgang til brugeren fra SSMS eller azure datastudio.  

Vær opmærksom på at det i laver i øvelsen her handler at lære brugerstyring i et database system. I en web applikation vil der være implementeret bruger autorisation i applikationen og det er applikationen der gives adgang til i databasen ved anvendelse af f.eks OAUTH implemeteret i en API.   

Når du arbejder skal du huske at anvende dit nuværende database server login har alle rettigheder og bruges derfor til at oprette nye, database server logins, database brugere samt til at ændre deres rettigheder. Man kan sige at du med dit nuværende login er database administrator.   

Du tester database brugere ved at logge ind med det oprettede server login og teste at database brugeren kun har rettigheder til det du har tildelt.  
Dette gøres ved at teste det der bør virke og derefter det der ikke bør virke. 

1. Lav et nyt login til din SQL Server, dette login skal ikke have rettigheder til at gøre noget på din server
  ![](images/db_security_settings_sp_user_add_0.png)
2. Opret brugeren som vist herunder
  ![](images/db_security_settings_sp_user_add_01.png) Password skal indeholde 8 karakterer, 1 specialtegn og 1 tal. F.eks `Password123.` **HUSK PASSWORD TIL SENERE**
3. Opret en bruger i din World database
  ![](images/db_security_settings_sp_user_add_02.png)
4. Giv brugeren et navn og indstil som herunder, brugeren skal kun have rettigheder til at eksekvere stored procedures (du vælger hvilke i securables med `Add...` knappen)
  ![](images/db_security_settings_sp_user_add_5.png)
  **Securables indstillinger eksempel**
  ![](images/db_security_settings_sp_user_add_6.png)
5. Lav en ny forbindelse til serveren og brug den bruger du har oprettet (brugernavn og password)
  ![](images/db_security_logged_in.png)
6. Lav en ny query `.sql` fil og afprøv forskellige forespørgsler, f.eks  
`EXEC spSumPerRegion 100000`  
der virker i eksemplet fordi jeg har givet brugeren rettigheder til den storep procedure der hedder spSumPerRegion.  
7. Verificer at brugeren ikke kan andet end at eksekvere stored procedures ved at køre andre forespørgsler, f.eks en select der burde fejle som vist herunder:
  ![](images/db_security_settings_test_permissions.png)

## Links

- 11 Steps to Secure SQL in 2022 [https://www.upguard.com/blog/11-steps-to-secure-sql](https://www.upguard.com/blog/11-steps-to-secure-sql)
- Microsoft SQl security dokumentation [https://learn.microsoft.com/en-us/sql/relational-databases/security](https://learn.microsoft.com/en-us/sql/relational-databases/security)