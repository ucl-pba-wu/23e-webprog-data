SELECT SYSDATETIME()
SELECT GETDATE()
SELECT SYSDATETIMEOFFSET()
SELECT DATEDIFF(year, '2018-12-31', '2019-05-01') as DateDiff;

SELECT ROUND(1.298, 1);
--Result: 1.3

SELECT CEILING(41.16);
--Result: 42

SELECT POWER(2, 2)

SELECT SUM(SupplierID * CategoryID) AS etellerandet
FROM Products 

USE Northwind
GO

SELECT CONCAT(firstname, ' ', lastname) AS fullname
FROM employees

SELECT COUNT(*) FROM employees;

USE World
GO

SELECT count(*) FROM country;
SELECT count(indepyear) FROM country;
SELECT count(DISTINCT continent) FROM country;

SELECT AVG(population)
FROM country
WHERE region = 'Western Europe';

SELECT MIN(population) AS MinimumPopulation, MAX(population) AS MaximumPopulation
FROM country
WHERE region = 'Western Europe';

