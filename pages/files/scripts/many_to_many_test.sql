CREATE DATABASE Many_test;
GO

USE Many_test;

CREATE TABLE Book (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    Title VARCHAR(255) NOT NULL,
    PublishDate DATE
)

CREATE TABLE Author (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    Firstname VARCHAR(255),
    Lastname VARCHAR(255)
)

CREATE TABLE BookHasAuthor (
    BookID INT,
    AuthorID INT,
    PRIMARY KEY (BookID, AuthorID),
    FOREIGN KEY (BookID) REFERENCES Book(ID),
    FOREIGN KEY (AuthorID) REFERENCES Author(ID)
);

INSERT INTO Book(Title, PublishDate)
VALUES 
        ('Det forsømte forår', '1940-07-15'),
        ('Den forsvundne fuldmægtig', '1938-02-10'),
        ('Den døde mand', '1937-03-04'),
        ('Idealister', '1945-04-01'),
        ('Gertrud', '1910-01-01'),
        ('Der Steppenwolf', '1927-02-27'),
        ('Siddharta', '1922-03-18')

INSERT INTO Author (FirstName, Lastname)
VALUES
        ('Hans', 'Scherfig'),
        ('Hermann', 'Hesse')

INSERT INTO BookHasAuthor(BookID, AuthorID)
VALUES
        (1,1), (2,1), (3,1), (4,1), (5,2), (6,2), (7,2)