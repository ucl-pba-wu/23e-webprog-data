USE Northwind;
GO

BEGIN TRY
  BEGIN TRANSACTION;

  DECLARE @neworderid AS INT;

  -- Insert a new order into the Orders table
  INSERT INTO Orders (...)VALUES (...);

  -- Save the new order ID in a variable
  SET @neworderid = SCOPE_IDENTITY();

  -- Insert order lines for the new order into OrderDetails
  INSERT INTO OrderDetails (...)
    VALUES(@neworderid, 11, 14.00, 12, 0.000),
          (@neworderid, 42, 9.80, 10, 0.000);

  COMMIT TRANSACTION;
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION;
END CATCH;