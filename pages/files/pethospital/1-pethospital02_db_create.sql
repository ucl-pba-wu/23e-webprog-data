CREATE DATABASE PetHospital02;
GO

USE PetHospital02;

CREATE TABLE PetOwner (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    Firstname VARCHAR(50) NOT NULL,
    Lastname VARCHAR(50) NOT NULL,
    Phone VARCHAR(20),
    Email VARCHAR(50) NOT NULL
);

CREATE TABLE PetType (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    PetType VARCHAR(50) NOT NULL
);

CREATE TABLE Pet (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    PetName VARCHAR(50) NOT NULL,
    DateBirth DATE,
    PetOwnerID INT FOREIGN KEY REFERENCES PetOwner(ID),
    PetTypeID INT FOREIGN KEY REFERENCES PetType(ID)
);

CREATE TABLE Treatment (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    Title VARCHAR(100) NOT NULL,
    Summary VARCHAR(1000)
);

CREATE TABLE Visit (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    VisitDateTime SMALLDATETIME,
    PetID INT FOREIGN KEY REFERENCES Pet(ID)
);


CREATE TABLE VisitHasTreatment (
    VisitID INT,
    TreatmentID INT,
    PRIMARY KEY (VisitID, TreatmentID),
    FOREIGN KEY (VisitID) REFERENCES Visit(ID),
    FOREIGN KEY (TreatmentID) REFERENCES Treatment(ID)
);


-- ADD 2 more tables, Invoice and TreatmentPrice
-- ADD Foreign Key Ref to Visit
CREATE TABLE Invoice (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    InvoiceDate SMALLDATETIME,
    AmountReceived BIT
);

CREATE TABLE TreatmentPrice (
    TreatmentID INT,
    PetTypeID INT,
    Price DECIMAL(9,2),
    PRIMARY KEY (TreatmentID, PetTypeID),
    FOREIGN KEY (TreatmentID) REFERENCES Treatment(ID),
    FOREIGN KEY (PetTypeID) REFERENCES PetType(ID)
);

ALTER TABLE Visit
    ADD InvoiceID INT FOREIGN KEY REFERENCES Invoice(ID)
GO

