USE PetHospital;

/* FILL THE TABLES WITH DUMMY DATA*/

insert into	PetOwner (FirstName, LastName, Phone, Email)
values	('Peter', 'Jennings', '+4523456543', 'pj@google.com'),
		('Joe', 'Dough', '+4598263742', 'joed@google.com'),
		('Donald', 'Trump', '5551234', 'maga@dt.usa'),
		('Anthony', 'Hopkins', '79139845666', 'sol@gmail.com'),
		('Paula', 'Jennings', '+4598236482', 'pj@gmail.com');

insert into	PetType(PetType)
values ('Cat'), ('Dog'), ('Fish'), ('Rhino'), ('Snake'); 

insert into	Pet (PetName, DOB, PetTypeID, PetOwnerID)
values	('Oscar', '2011-08-25', 3, 1),
		('Buller', '2009-03-14', 2, 1),
		('Miv', '2001-02-17', 2, 3),
		('Bruno', '1958-12-23', 5, 2),
		('Ollie', '2001-04-01', 4, 4),
		('OllieMommy', '1978-06-02', 4, 4);

INSERT INTO Treatment(Title)
VALUES 
	('Eye Wash'),
	('Rabies Vaccination'),
	('Annual Checkup');

INSERT INTO Visit (VisitDateTime, PetID)
VALUES 
	('2022-08-01 10:00', 1),
	('2022-08-03 12:00', 2),
	('2022-08-03 14:00', 3),
	('2022-08-03 15:30', 4),
	('2022-08-05 09:30', 4);

INSERT INTO VisitHasTreatment(VisitID, TreatmentID)
VALUES (1, 1), (1,3), (2, 2), (3,1), (4,2), (4,1), (5,3);
