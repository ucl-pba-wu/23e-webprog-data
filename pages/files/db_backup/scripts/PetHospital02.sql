USE [master]
GO
/****** Object:  Database [PetHospital02]    Script Date: 2023-09-08 12:35:18 ******/
CREATE DATABASE [PetHospital02]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PetHospital02', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\PetHospital02.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PetHospital02_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\PetHospital02_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [PetHospital02] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PetHospital02].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PetHospital02] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PetHospital02] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PetHospital02] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PetHospital02] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PetHospital02] SET ARITHABORT OFF 
GO
ALTER DATABASE [PetHospital02] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [PetHospital02] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PetHospital02] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PetHospital02] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PetHospital02] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PetHospital02] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PetHospital02] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PetHospital02] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PetHospital02] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PetHospital02] SET  ENABLE_BROKER 
GO
ALTER DATABASE [PetHospital02] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PetHospital02] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PetHospital02] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PetHospital02] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PetHospital02] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PetHospital02] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PetHospital02] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PetHospital02] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PetHospital02] SET  MULTI_USER 
GO
ALTER DATABASE [PetHospital02] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PetHospital02] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PetHospital02] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PetHospital02] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PetHospital02] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [PetHospital02] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [PetHospital02] SET QUERY_STORE = OFF
GO
USE [PetHospital02]
GO
/****** Object:  Table [dbo].[Invoice]    Script Date: 2023-09-08 12:35:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceDate] [smalldatetime] NULL,
	[AmountReceived] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pet]    Script Date: 2023-09-08 12:35:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pet](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PetName] [varchar](50) NOT NULL,
	[DateBirth] [date] NULL,
	[PetOwnerID] [int] NULL,
	[PetTypeID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PetOwner]    Script Date: 2023-09-08 12:35:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PetOwner](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Firstname] [varchar](50) NOT NULL,
	[Lastname] [varchar](50) NOT NULL,
	[Phone] [varchar](20) NULL,
	[Email] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PetType]    Script Date: 2023-09-08 12:35:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PetType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PetType] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Treatment]    Script Date: 2023-09-08 12:35:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Treatment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](100) NOT NULL,
	[Summary] [varchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TreatmentPrice]    Script Date: 2023-09-08 12:35:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TreatmentPrice](
	[TreatmentID] [int] NOT NULL,
	[PetTypeID] [int] NOT NULL,
	[Price] [decimal](9, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[TreatmentID] ASC,
	[PetTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Visit]    Script Date: 2023-09-08 12:35:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Visit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VisitDateTime] [smalldatetime] NULL,
	[PetID] [int] NULL,
	[InvoiceID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VisitHasTreatment]    Script Date: 2023-09-08 12:35:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VisitHasTreatment](
	[VisitID] [int] NOT NULL,
	[TreatmentID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[VisitID] ASC,
	[TreatmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Invoice] ON 

INSERT [dbo].[Invoice] ([ID], [InvoiceDate], [AmountReceived]) VALUES (1, CAST(N'2020-08-31T00:00:00' AS SmallDateTime), 1)
SET IDENTITY_INSERT [dbo].[Invoice] OFF
GO
SET IDENTITY_INSERT [dbo].[Pet] ON 

INSERT [dbo].[Pet] ([ID], [PetName], [DateBirth], [PetOwnerID], [PetTypeID]) VALUES (1, N'Birdy', CAST(N'2018-10-24' AS Date), 1, 1)
INSERT [dbo].[Pet] ([ID], [PetName], [DateBirth], [PetOwnerID], [PetTypeID]) VALUES (2, N'Mis', CAST(N'2016-01-01' AS Date), 1, 2)
INSERT [dbo].[Pet] ([ID], [PetName], [DateBirth], [PetOwnerID], [PetTypeID]) VALUES (3, N'Fishy', CAST(N'2019-12-24' AS Date), 2, 4)
INSERT [dbo].[Pet] ([ID], [PetName], [DateBirth], [PetOwnerID], [PetTypeID]) VALUES (4, N'Wuff', CAST(N'2015-08-02' AS Date), 3, 3)
INSERT [dbo].[Pet] ([ID], [PetName], [DateBirth], [PetOwnerID], [PetTypeID]) VALUES (5, N'Barky', CAST(N'2013-02-28' AS Date), 2, 2)
SET IDENTITY_INSERT [dbo].[Pet] OFF
GO
SET IDENTITY_INSERT [dbo].[PetOwner] ON 

INSERT [dbo].[PetOwner] ([ID], [Firstname], [Lastname], [Phone], [Email]) VALUES (1, N'Bernie', N'Besenhofer', N'99999999', N'berb@ucl.dk')
INSERT [dbo].[PetOwner] ([ID], [Firstname], [Lastname], [Phone], [Email]) VALUES (2, N'Harry', N'Jensen', N'88888888', N'harry@ucl.dk')
INSERT [dbo].[PetOwner] ([ID], [Firstname], [Lastname], [Phone], [Email]) VALUES (3, N'Dobby', N'Larsen', N'77777777', N'dobby@ucl.dk')
SET IDENTITY_INSERT [dbo].[PetOwner] OFF
GO
SET IDENTITY_INSERT [dbo].[PetType] ON 

INSERT [dbo].[PetType] ([ID], [PetType]) VALUES (1, N'Bird')
INSERT [dbo].[PetType] ([ID], [PetType]) VALUES (2, N'Cat')
INSERT [dbo].[PetType] ([ID], [PetType]) VALUES (3, N'Dog')
INSERT [dbo].[PetType] ([ID], [PetType]) VALUES (4, N'Fish')
SET IDENTITY_INSERT [dbo].[PetType] OFF
GO
SET IDENTITY_INSERT [dbo].[Treatment] ON 

INSERT [dbo].[Treatment] ([ID], [Title], [Summary]) VALUES (1, N'Eye Wash', NULL)
INSERT [dbo].[Treatment] ([ID], [Title], [Summary]) VALUES (2, N'Rabies Vaccination', NULL)
INSERT [dbo].[Treatment] ([ID], [Title], [Summary]) VALUES (3, N'Annual Checkup', NULL)
SET IDENTITY_INSERT [dbo].[Treatment] OFF
GO
INSERT [dbo].[TreatmentPrice] ([TreatmentID], [PetTypeID], [Price]) VALUES (1, 1, CAST(400.00 AS Decimal(9, 2)))
INSERT [dbo].[TreatmentPrice] ([TreatmentID], [PetTypeID], [Price]) VALUES (1, 2, CAST(300.00 AS Decimal(9, 2)))
INSERT [dbo].[TreatmentPrice] ([TreatmentID], [PetTypeID], [Price]) VALUES (1, 3, CAST(200.00 AS Decimal(9, 2)))
INSERT [dbo].[TreatmentPrice] ([TreatmentID], [PetTypeID], [Price]) VALUES (2, 1, CAST(40.00 AS Decimal(9, 2)))
INSERT [dbo].[TreatmentPrice] ([TreatmentID], [PetTypeID], [Price]) VALUES (2, 2, CAST(30.00 AS Decimal(9, 2)))
INSERT [dbo].[TreatmentPrice] ([TreatmentID], [PetTypeID], [Price]) VALUES (2, 3, CAST(20.00 AS Decimal(9, 2)))
INSERT [dbo].[TreatmentPrice] ([TreatmentID], [PetTypeID], [Price]) VALUES (2, 4, CAST(10.00 AS Decimal(9, 2)))
GO
SET IDENTITY_INSERT [dbo].[Visit] ON 

INSERT [dbo].[Visit] ([ID], [VisitDateTime], [PetID], [InvoiceID]) VALUES (1, CAST(N'2020-08-01T10:00:00' AS SmallDateTime), 1, NULL)
INSERT [dbo].[Visit] ([ID], [VisitDateTime], [PetID], [InvoiceID]) VALUES (2, CAST(N'2020-08-03T12:00:00' AS SmallDateTime), 2, NULL)
INSERT [dbo].[Visit] ([ID], [VisitDateTime], [PetID], [InvoiceID]) VALUES (3, CAST(N'2020-08-03T14:00:00' AS SmallDateTime), 3, 1)
INSERT [dbo].[Visit] ([ID], [VisitDateTime], [PetID], [InvoiceID]) VALUES (4, CAST(N'2020-08-03T15:30:00' AS SmallDateTime), 4, NULL)
INSERT [dbo].[Visit] ([ID], [VisitDateTime], [PetID], [InvoiceID]) VALUES (5, CAST(N'2020-08-05T09:30:00' AS SmallDateTime), 4, NULL)
SET IDENTITY_INSERT [dbo].[Visit] OFF
GO
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (1, 1)
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (1, 3)
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (2, 2)
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (3, 1)
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (4, 1)
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (4, 2)
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (5, 3)
GO
ALTER TABLE [dbo].[Pet]  WITH CHECK ADD FOREIGN KEY([PetOwnerID])
REFERENCES [dbo].[PetOwner] ([ID])
GO
ALTER TABLE [dbo].[Pet]  WITH CHECK ADD FOREIGN KEY([PetTypeID])
REFERENCES [dbo].[PetType] ([ID])
GO
ALTER TABLE [dbo].[TreatmentPrice]  WITH CHECK ADD FOREIGN KEY([PetTypeID])
REFERENCES [dbo].[PetType] ([ID])
GO
ALTER TABLE [dbo].[TreatmentPrice]  WITH CHECK ADD FOREIGN KEY([TreatmentID])
REFERENCES [dbo].[Treatment] ([ID])
GO
ALTER TABLE [dbo].[Visit]  WITH CHECK ADD FOREIGN KEY([InvoiceID])
REFERENCES [dbo].[Invoice] ([ID])
GO
ALTER TABLE [dbo].[Visit]  WITH CHECK ADD FOREIGN KEY([PetID])
REFERENCES [dbo].[Pet] ([ID])
GO
ALTER TABLE [dbo].[VisitHasTreatment]  WITH CHECK ADD FOREIGN KEY([TreatmentID])
REFERENCES [dbo].[Treatment] ([ID])
GO
ALTER TABLE [dbo].[VisitHasTreatment]  WITH CHECK ADD FOREIGN KEY([VisitID])
REFERENCES [dbo].[Visit] ([ID])
GO
USE [master]
GO
ALTER DATABASE [PetHospital02] SET  READ_WRITE 
GO
