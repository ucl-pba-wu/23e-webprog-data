USE [master]
GO
/****** Object:  Database [PetHospital]    Script Date: 2023-09-08 12:34:31 ******/
CREATE DATABASE [PetHospital]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PetHospital', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\PetHospital.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PetHospital_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\PetHospital_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [PetHospital] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PetHospital].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PetHospital] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PetHospital] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PetHospital] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PetHospital] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PetHospital] SET ARITHABORT OFF 
GO
ALTER DATABASE [PetHospital] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [PetHospital] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PetHospital] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PetHospital] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PetHospital] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PetHospital] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PetHospital] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PetHospital] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PetHospital] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PetHospital] SET  ENABLE_BROKER 
GO
ALTER DATABASE [PetHospital] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PetHospital] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PetHospital] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PetHospital] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PetHospital] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PetHospital] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PetHospital] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PetHospital] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PetHospital] SET  MULTI_USER 
GO
ALTER DATABASE [PetHospital] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PetHospital] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PetHospital] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PetHospital] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PetHospital] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [PetHospital] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [PetHospital] SET QUERY_STORE = OFF
GO
USE [PetHospital]
GO
/****** Object:  Table [dbo].[Pet]    Script Date: 2023-09-08 12:34:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pet](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PetName] [varchar](50) NOT NULL,
	[DOB] [date] NULL,
	[PetOwnerID] [int] NOT NULL,
	[PetTypeID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PetOwner]    Script Date: 2023-09-08 12:34:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PetOwner](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Firstname] [varchar](50) NOT NULL,
	[Lastname] [varchar](50) NOT NULL,
	[Phone] [varchar](20) NULL,
	[Email] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PetType]    Script Date: 2023-09-08 12:34:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PetType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PetType] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Treatment]    Script Date: 2023-09-08 12:34:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Treatment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Visit]    Script Date: 2023-09-08 12:34:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Visit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VisitDateTime] [smalldatetime] NOT NULL,
	[PetID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VisitHasTreatment]    Script Date: 2023-09-08 12:34:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VisitHasTreatment](
	[VisitID] [int] NOT NULL,
	[TreatmentID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[VisitID] ASC,
	[TreatmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Pet] ON 

INSERT [dbo].[Pet] ([ID], [PetName], [DOB], [PetOwnerID], [PetTypeID]) VALUES (1, N'Oscar', CAST(N'2011-08-25' AS Date), 1, 3)
INSERT [dbo].[Pet] ([ID], [PetName], [DOB], [PetOwnerID], [PetTypeID]) VALUES (2, N'Buller', CAST(N'2009-03-14' AS Date), 1, 2)
INSERT [dbo].[Pet] ([ID], [PetName], [DOB], [PetOwnerID], [PetTypeID]) VALUES (3, N'Miv', CAST(N'2001-02-17' AS Date), 3, 2)
INSERT [dbo].[Pet] ([ID], [PetName], [DOB], [PetOwnerID], [PetTypeID]) VALUES (4, N'Bruno', CAST(N'1958-12-23' AS Date), 2, 5)
INSERT [dbo].[Pet] ([ID], [PetName], [DOB], [PetOwnerID], [PetTypeID]) VALUES (5, N'Ollie', CAST(N'2001-04-01' AS Date), 4, 4)
INSERT [dbo].[Pet] ([ID], [PetName], [DOB], [PetOwnerID], [PetTypeID]) VALUES (6, N'OllieMommy', CAST(N'1978-06-02' AS Date), 4, 4)
SET IDENTITY_INSERT [dbo].[Pet] OFF
GO
SET IDENTITY_INSERT [dbo].[PetOwner] ON 

INSERT [dbo].[PetOwner] ([ID], [Firstname], [Lastname], [Phone], [Email]) VALUES (1, N'Peter', N'Jennings', N'+4523456543', N'pj@google.com')
INSERT [dbo].[PetOwner] ([ID], [Firstname], [Lastname], [Phone], [Email]) VALUES (2, N'Joe', N'Dough', N'+4598263742', N'joed@google.com')
INSERT [dbo].[PetOwner] ([ID], [Firstname], [Lastname], [Phone], [Email]) VALUES (3, N'Donald', N'Trump', N'5551234', N'maga@dt.usa')
INSERT [dbo].[PetOwner] ([ID], [Firstname], [Lastname], [Phone], [Email]) VALUES (4, N'Anthony', N'Hopkins', N'79139845666', N'sol@gmail.com')
INSERT [dbo].[PetOwner] ([ID], [Firstname], [Lastname], [Phone], [Email]) VALUES (5, N'Paula', N'Jennings', N'+4598236482', N'pj@gmail.com')
SET IDENTITY_INSERT [dbo].[PetOwner] OFF
GO
SET IDENTITY_INSERT [dbo].[PetType] ON 

INSERT [dbo].[PetType] ([ID], [PetType]) VALUES (1, N'Cat')
INSERT [dbo].[PetType] ([ID], [PetType]) VALUES (2, N'Dog')
INSERT [dbo].[PetType] ([ID], [PetType]) VALUES (3, N'Fish')
INSERT [dbo].[PetType] ([ID], [PetType]) VALUES (4, N'Rhino')
INSERT [dbo].[PetType] ([ID], [PetType]) VALUES (5, N'Snake')
SET IDENTITY_INSERT [dbo].[PetType] OFF
GO
SET IDENTITY_INSERT [dbo].[Treatment] ON 

INSERT [dbo].[Treatment] ([ID], [Title]) VALUES (1, N'Eye Wash')
INSERT [dbo].[Treatment] ([ID], [Title]) VALUES (2, N'Rabies Vaccination')
INSERT [dbo].[Treatment] ([ID], [Title]) VALUES (3, N'Annual Checkup')
SET IDENTITY_INSERT [dbo].[Treatment] OFF
GO
SET IDENTITY_INSERT [dbo].[Visit] ON 

INSERT [dbo].[Visit] ([ID], [VisitDateTime], [PetID]) VALUES (1, CAST(N'2022-08-01T10:00:00' AS SmallDateTime), 1)
INSERT [dbo].[Visit] ([ID], [VisitDateTime], [PetID]) VALUES (2, CAST(N'2022-08-03T12:00:00' AS SmallDateTime), 2)
INSERT [dbo].[Visit] ([ID], [VisitDateTime], [PetID]) VALUES (3, CAST(N'2022-08-03T14:00:00' AS SmallDateTime), 3)
INSERT [dbo].[Visit] ([ID], [VisitDateTime], [PetID]) VALUES (4, CAST(N'2022-08-03T15:30:00' AS SmallDateTime), 4)
INSERT [dbo].[Visit] ([ID], [VisitDateTime], [PetID]) VALUES (5, CAST(N'2022-08-05T09:30:00' AS SmallDateTime), 4)
SET IDENTITY_INSERT [dbo].[Visit] OFF
GO
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (1, 1)
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (1, 3)
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (2, 2)
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (3, 1)
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (4, 1)
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (4, 2)
INSERT [dbo].[VisitHasTreatment] ([VisitID], [TreatmentID]) VALUES (5, 3)
GO
ALTER TABLE [dbo].[Pet]  WITH CHECK ADD FOREIGN KEY([PetOwnerID])
REFERENCES [dbo].[PetOwner] ([ID])
GO
ALTER TABLE [dbo].[Pet]  WITH CHECK ADD FOREIGN KEY([PetTypeID])
REFERENCES [dbo].[PetType] ([ID])
GO
ALTER TABLE [dbo].[Visit]  WITH CHECK ADD FOREIGN KEY([PetID])
REFERENCES [dbo].[Pet] ([ID])
GO
ALTER TABLE [dbo].[VisitHasTreatment]  WITH CHECK ADD FOREIGN KEY([TreatmentID])
REFERENCES [dbo].[Treatment] ([ID])
GO
ALTER TABLE [dbo].[VisitHasTreatment]  WITH CHECK ADD FOREIGN KEY([VisitID])
REFERENCES [dbo].[Visit] ([ID])
GO
USE [master]
GO
ALTER DATABASE [PetHospital] SET  READ_WRITE 
GO
