USE [master]
GO
/****** Object:  Database [our_first_database]    Script Date: 2023-09-08 12:33:33 ******/
CREATE DATABASE [our_first_database]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'our_first_database', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\our_first_database.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'our_first_database_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\our_first_database_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [our_first_database] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [our_first_database].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [our_first_database] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [our_first_database] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [our_first_database] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [our_first_database] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [our_first_database] SET ARITHABORT OFF 
GO
ALTER DATABASE [our_first_database] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [our_first_database] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [our_first_database] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [our_first_database] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [our_first_database] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [our_first_database] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [our_first_database] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [our_first_database] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [our_first_database] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [our_first_database] SET  ENABLE_BROKER 
GO
ALTER DATABASE [our_first_database] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [our_first_database] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [our_first_database] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [our_first_database] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [our_first_database] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [our_first_database] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [our_first_database] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [our_first_database] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [our_first_database] SET  MULTI_USER 
GO
ALTER DATABASE [our_first_database] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [our_first_database] SET DB_CHAINING OFF 
GO
ALTER DATABASE [our_first_database] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [our_first_database] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [our_first_database] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [our_first_database] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [our_first_database] SET QUERY_STORE = OFF
GO
USE [our_first_database]
GO
/****** Object:  Table [dbo].[city]    Script Date: 2023-09-08 12:33:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[city](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[city_name] [char](128) NOT NULL,
	[lat] [decimal](9, 6) NOT NULL,
	[long] [decimal](9, 6) NOT NULL,
	[country_id] [int] NOT NULL,
 CONSTRAINT [city_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[country]    Script Date: 2023-09-08 12:33:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[country](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[country_name] [char](128) NOT NULL,
	[country_name_eng] [char](128) NOT NULL,
	[country_code] [char](8) NOT NULL,
 CONSTRAINT [country_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[city] ON 

INSERT [dbo].[city] ([id], [city_name], [lat], [long], [country_id]) VALUES (1, N'Berlin                                                                                                                          ', CAST(52.520008 AS Decimal(9, 6)), CAST(13.404954 AS Decimal(9, 6)), 1)
INSERT [dbo].[city] ([id], [city_name], [lat], [long], [country_id]) VALUES (2, N'Belgrade                                                                                                                        ', CAST(44.787197 AS Decimal(9, 6)), CAST(20.457273 AS Decimal(9, 6)), 2)
INSERT [dbo].[city] ([id], [city_name], [lat], [long], [country_id]) VALUES (3, N'Zagreb                                                                                                                          ', CAST(45.815399 AS Decimal(9, 6)), CAST(15.966568 AS Decimal(9, 6)), 3)
INSERT [dbo].[city] ([id], [city_name], [lat], [long], [country_id]) VALUES (4, N'New York                                                                                                                        ', CAST(40.730610 AS Decimal(9, 6)), CAST(-73.935242 AS Decimal(9, 6)), 4)
INSERT [dbo].[city] ([id], [city_name], [lat], [long], [country_id]) VALUES (5, N'Los Angeles                                                                                                                     ', CAST(34.052235 AS Decimal(9, 6)), CAST(-118.243683 AS Decimal(9, 6)), 4)
INSERT [dbo].[city] ([id], [city_name], [lat], [long], [country_id]) VALUES (6, N'Warsaw                                                                                                                          ', CAST(52.237049 AS Decimal(9, 6)), CAST(21.017532 AS Decimal(9, 6)), 5)
SET IDENTITY_INSERT [dbo].[city] OFF
GO
SET IDENTITY_INSERT [dbo].[country] ON 

INSERT [dbo].[country] ([id], [country_name], [country_name_eng], [country_code]) VALUES (1, N'Deutschland                                                                                                                     ', N'Germany                                                                                                                         ', N'DEU     ')
INSERT [dbo].[country] ([id], [country_name], [country_name_eng], [country_code]) VALUES (2, N'Srbija                                                                                                                          ', N'Serbia                                                                                                                          ', N'SRB     ')
INSERT [dbo].[country] ([id], [country_name], [country_name_eng], [country_code]) VALUES (3, N'Hrvatska                                                                                                                        ', N'Croatia                                                                                                                         ', N'HRV     ')
INSERT [dbo].[country] ([id], [country_name], [country_name_eng], [country_code]) VALUES (4, N'United Stated of America                                                                                                        ', N'United Stated of America                                                                                                        ', N'USA     ')
INSERT [dbo].[country] ([id], [country_name], [country_name_eng], [country_code]) VALUES (5, N'Polska                                                                                                                          ', N'Poland                                                                                                                          ', N'POL     ')
SET IDENTITY_INSERT [dbo].[country] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [country_ak_1]    Script Date: 2023-09-08 12:33:33 ******/
ALTER TABLE [dbo].[country] ADD  CONSTRAINT [country_ak_1] UNIQUE NONCLUSTERED 
(
	[country_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [country_ak_2]    Script Date: 2023-09-08 12:33:33 ******/
ALTER TABLE [dbo].[country] ADD  CONSTRAINT [country_ak_2] UNIQUE NONCLUSTERED 
(
	[country_name_eng] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [country_ak_3]    Script Date: 2023-09-08 12:33:33 ******/
ALTER TABLE [dbo].[country] ADD  CONSTRAINT [country_ak_3] UNIQUE NONCLUSTERED 
(
	[country_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[city]  WITH CHECK ADD  CONSTRAINT [city_country] FOREIGN KEY([country_id])
REFERENCES [dbo].[country] ([id])
GO
ALTER TABLE [dbo].[city] CHECK CONSTRAINT [city_country]
GO
USE [master]
GO
ALTER DATABASE [our_first_database] SET  READ_WRITE 
GO
