-- 1. Show all cities in the district of Hubei, that have a name starting with X
SELECT *
FROM City 
WHERE name LIKE 'X%' AND District = 'Hubei';

-- 2. Show the top 10 cities with the highest population. Show name and population only
SELECT TOP(10) Name, Population
FROM City
ORDER BY Population DESC

-- 3. List all cities in districts La Paz, West Java and Limburg ordered A-Z
SELECT *
FROM City
WHERE District IN ('La Paz', 'West Java', 'Limburg')
ORDER BY Name

-- 4. Show all regions in Europe (no duplicates)
SELECT DISTINCT Region
FROM Country
WHERE Continent = 'Europe'

-- 5. Show all country names with a surface area smaller than 100 or a life expectany of 80 or more
SELECT * 
FROM Country
WHERE SurfaceArea < 100 OR LifeExpectancy >= 80

-- 6. Show all countries that have no HeadOfState
SELECT *
FROM Country
WHERE HeadOfState IS NULL

-- 7. Show the country code of all countries where english is the official language
SELECT CountryCode
FROM CountryLanguage
WHERE IsOfficial = 'T' AND [Language] = 'English'

-- 8. Make up your own query (including question)






