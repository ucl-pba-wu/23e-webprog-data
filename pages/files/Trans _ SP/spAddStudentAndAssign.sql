create procedure addstudentandassign
@email varchar(100),
@courseid int
as
begin
	begin try
		begin transaction
			insert into student
			values (@email);

			insert into studenthascourse 
			values (SCOPE_IDENTITY(), @courseid);
		commit transaction
	end try
	begin catch
		Print 'There was an error. FIX IT';
		rollback transaction
	end catch
end



