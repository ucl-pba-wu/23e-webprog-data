db.clients.insertMany([
    {
        firstname: "Paul",
        surname: "Miller",
        city: "London",
        cars: [{
            model: "Bentley",
            year: 1973,
            value: 100000
        },
        {
            model: "Rolls Royce",
            year: 1965,
            value: 330000
        }
        ]
    },
    {
        firstname: "Alvaro",
        surname: "Ortega",
        city: "Valenica"
    },
    {
        firstname: "Urs",
        surname: "Huber",
        city: "Zurich",
        cars: [{
            model: "Smart",
            year: 1999,
            value: 2000
        }]
    }, {
        firstname: "Gaston",
        surname: "Blanc",
        city: "Paris",
        cars: [{
            model: "Renault",
            year: 1998,
            value: 2000
        },
        {
            model: "Renault",
            year: 2001,
            value: 7000
        },
        {
            model: "Peugeot",
            year: 1993,
            value: 500
        }
        ]
    },
    {
        firstname: "Fabrizio",
        surname: "Bertolini",
        city: "Rome",
        cars: [{
            model: "Ferrari",
            year: 2005,
            value: 150000
        }]
    }
])