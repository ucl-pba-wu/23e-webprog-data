show dbs;
use test;
show collections;
db.dropDatabase();


// Insert
db.products.insertMany([{
    "name": "Cat food",
    "price": 400.00,
    "qty": 40,
    "supplier": "AB cat food"
}, {
    "name": "Dog food",
    "price": 99.10,
    "qty": 50,
    "ingredients": ["Flour", "Salt", "Meat"],
    "info": {
        "summary": "Summary goes here ...",
        "full": "Description goes here ..."
    }
}]);

db.products.count();

// Find
db.products.find({});
db.products.find().pretty();

db.products.find({ name: "Cat food" });
db.products.find({ qty: 20 });
db.products.find({ qty: { $gt: 40, $lt: 100 } })
db.products.find({ "info.summary": { $exists: true } })
db.products.find({ "ingredients": "Meat" }, { name: true })
db.products.find({ "ingredients": { $all: ["Meat", "Salt"] } }, { name: true })
db.products.find({ "ingredients.0": "Meat" }, { name: true })
db.products.find({ "name": { $regex: /^Cat/ } }, { name: true })
db.products.find({ "name": { $regex: /^Cat/i } }, { name: true })
db.products.find({
    $or: [
        { price: { $gt: 99 } },
        { qty: 50 }
    ]
})

// .sort({ name: 1 })
// .limit(5)