// Client collection

// Find all cars for firstname 'Gaston'
db.clients.find({
    "firstname": "Gaston"
},
    {
        cars: 1, _id: 0
    });

// Show the city of Paul Miller
db.clients.find({
    "firstname": "Paul", "surname": "Miller"
},
    {
        city: 1
    })

// Find cars with a value bigger than 100000
db.clients.find({
    "cars.value": { $gt: 100000 }
})

// Find all clients with a surname starting with 'B'
db.clients.find({
    "surname": /^B/
})

// Find owners of all 'Renault' cars
db.clients.find({
    "cars.model": "Renault"
},
    {
        surname: 1, firstname: 1
    })

// Find car models and values that were built between the years 1990-2000
db.clients.find({
    "cars.year": { $gte: 1990, $lte: 2000 },
})

// Find all client names that live in Zurich or Rome
db.clients.find({
    $or: [{
        city: "Zurich"
    }, {
        city: "Rome"
    }
    ]
})


// Count the cars build in year 1999.
db.clients.find({
    "cars.year": 1999
})


// Split up car array
db.clients.aggregate([{
    $unwind: "$cars"
}])
