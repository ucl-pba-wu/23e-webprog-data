//1.Vis antal restauranter pr. køkken (cousine)

db.restaurants.aggregate([{$group : {_id : "$cuisine", total: { $sum: 1}}},])

//2.Vis antallet af restauranter pr. by, højeste antal skal være først og kun de 3 første resultater skal vises

db.restaurants.aggregate([
    {$group : {_id : "$address.city", restprcity: { $sum: 1}}},
    {$sort : { restprcity: -1}},
    {$limit: 3}
])

//3.Vis hvor mange gæster det samlede antal restauranter i Odense har plads til

db.restaurants.aggregate([
    {$match : {"address.city": "Odense"}},
    {$group: { "_id": "$address.Odense", capforOdense: {$sum: "$capacity"}}},
    {$project : {_id: 0}}
])        

//4.Vis den højeste max pris blandt alle restauranterne 

db.restaurants.aggregate([
    {$group : {_id : null, max_price_all: {$max: "$pricerange.max"}}}
])

//5.Vis antal inspektioner pr. inspetør 

db.restaurants.aggregate([
    {$unwind : "$inspection"},
    {$group : {_id : "$inspection.inspectedby", count: {$sum: 1}}},
    {$sort : { count: -1}}        
])

//6.Vis gennemsnits score pr. restaurant

db.restaurants.aggregate([
    {$unwind : "$inspection"},
    {$group: {"_id" : {_id: "$_id", name: "$name"}, avgscore: {$avg: "$inspection.score"}}},
    {$project: { name: "$_id.name", avgscore: 1, _id: 0}}
])