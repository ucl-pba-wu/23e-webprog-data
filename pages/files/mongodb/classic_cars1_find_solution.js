// classic_cars clients collection
use classic_cars

// vis hele clients collection
db.clients.find()

// Find alle biler for personer med fornavnet "Gaston"
db.clients.find({"firstname": "Gaston"}, {cars: 1, _id: 0});

// Vis den by Paul Miller bor i
db.clients.find({"firstname": "Paul", "surname": "Miller"}, {city: 1, _id: 0})

// Find alle personer med et efternavn der starter med "B"
db.clients.find({"surname": /^B/})

// Find alle der ejer en "Renault"
db.clients.find({"cars.model": "Renault"},{surname: 1, firstname: 1, cars: 1})

// Find alle personer der bor i enten Zurich eller Rome
db.clients.find({$or: [{city: "Zurich"}, {city: "Rome"}]})

// Tæl hvor mange biler der er bygget i 1999
db.clients.count({"cars.year": 1999})
db.clients.find({"cars.year": 1999}).count()

// Split cars array
db.clients.aggregate([{$unwind: "$cars"}])